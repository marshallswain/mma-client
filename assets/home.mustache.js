(function(window) {
 can.view.preloadStringRenderer('home_components_auth_change-password_change-password_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<form class=\"well\" role=\"form\" can-submit=\"changePassword\"",can.view.pending({attrs: ['can-submit'], scope: scope,options: options}),">");___v1ew.push(
"\n\t<h3>Change Password</h3>\n\t<p>Enter and confirm your new password</p>");___v1ew.push(
"\n");___v1ew.push(
can.view.txt(
0,
'form',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"success"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"  <div class=\"bg-success\"><p>Password changed successfully.</p></div>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"  ");___v1ew.push(
can.view.txt(
0,
'form',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"passwordMismatch"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"\n  <div class=\"bg-danger\"><p>Passwords must match.</p></div>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"  ");___v1ew.push(
can.view.txt(
0,
'form',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"passwordTooShort"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"\n\t<div class=\"bg-danger\"><p>Password must be at least 6 characters long.</p></div>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"  <input type=\"password\" class=\"form-control top\" placeholder=\"New Password\" required can-value=\"password\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),"/>");___v1ew.push(
"\n  <input type=\"password\" class=\"form-control bottom\" placeholder=\"Confirm Password\" required can-value=\"password2\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),"/>");___v1ew.push(
"\n  <div class=\"signup-messages bg-danger\">");___v1ew.push(
"\n");___v1ew.push(
can.view.txt(
0,
'div',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"notVerified"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"      <p>That account has not been verified.  Please check your email and click the verification link.</p>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"  </div>\n  <br/>\n  <button class=\"btn btn-success\" type=\"submit\">Change Password</button>\n</form>");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_auth_login_login_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div class=\"title red\">\n  <div class=\"container\">\n    <h1>Enjoy the show</h1>\n  </div>\n</div>\n<div id=\"login\">\n  <div class=\"container\">\n    <div class=\"content\">");___v1ew.push(
"\n      <div class=\"form\">\n        <form can-submit=\"login\"",can.view.pending({attrs: ['can-submit'], scope: scope,options: options}),">");___v1ew.push(
"\n          <h4>Enter your email and password.</h4>\n          <div class=\"signup-messages bg-danger\">");___v1ew.push(
"\n");___v1ew.push(
can.view.txt(
0,
'div',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"notVerified"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"              <p>That account has not been verified.  Please check your email and click the verification link.</p>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"            ");___v1ew.push(
can.view.txt(
0,
'div',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"invalidLogin"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"\n              <p>Email address or password incorrect. <button class=\"btn btn-link\" can-click=\"resetPassword\"",can.view.pending({attrs: ['can-click'], scope: scope,options: options}),">");___v1ew.push(
"Reset Password</button></p>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"          </div>\n          <input type=\"text\" name=\"email\" placeholder=\"Email\" can-value=\"email\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),">");___v1ew.push(
"\n          <input type=\"password\" name=\"password\" placeholder=\"Password\" can-value=\"password\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),">");___v1ew.push(
"\n          <span><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'forgot')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Forgot your password?</a></span><br>\n          <input type=\"submit\" value=\"Sign In\" class=\"btn btn-black1\">");___v1ew.push(
"\n          <a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'signup')));___v1ew.push(
"\" class=\"btn btn-block btn-link cancel\" type=\"button\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"No account? Set one up.</a>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_auth_my-account_my-account_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div class=\"container\">\n\t<div class=\"page\" id=\"my-account\">\n\t\t<h2>My Account</h2>\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-md-8\">\n\t\t\t\t<p>If there were any details about your account to show, they would be shown here.</p>\n\t\t\t</div>\n\t\t\t<div class=\"col-md-4\">\n\t\t\t\t<change-password",can.view.pending({tagName:'change-password',scope: scope,options: options}));___v1ew.push(
"></change-password>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_auth_passwordchange_passwordchange_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"\n<div class=\"col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3\">\n  <form id=\"loginform\" class=\"form-signin\" role=\"form\" can-submit=\"send\"",can.view.pending({attrs: ['can-submit'], scope: scope,options: options}),">");___v1ew.push(
"\n    <h2>Password Change Code</h2>\n    <p>Enter the code from the email we sent you.</p>\n    <input type=\"text\" class=\"form-control\" placeholder=\"Password Reset Code\" required  can-value=\"secret\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),"/>");___v1ew.push(
"\n    <br/>\n    <p>Enter and confirm your new password.</p>\n    <input type=\"password\" class=\"form-control top\" placeholder=\"New Password\" autofocus=\"\" required can-value=\"password\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),"/>");___v1ew.push(
"\n    <input type=\"password\" class=\"form-control bottom\" placeholder=\"Confirm New Password\" required can-value=\"password2\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),"/>");___v1ew.push(
"\n    <div class=\"signup-messages bg-danger\">");___v1ew.push(
"\n");___v1ew.push(
can.view.txt(
0,
'div',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"invalidCode"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"        <p>That code has already been used. Please delete the email and <a href=\"#\" can-click=\"tryagain\"",can.view.pending({attrs: ['can-click'], scope: scope,options: options}),">");___v1ew.push(
" request a new one here</a>.</p>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"    </div>\n    <br/>\n    <button class=\"btn btn-lg btn-success btn-block\" type=\"submit\">Reset Password</button>\n    <button class=\"btn btn-block btn-link cancel\" type=\"button\" can-click=\"tryagain\"",can.view.pending({attrs: ['can-click'], scope: scope,options: options}),">");___v1ew.push(
"Didn't get the email? Try again</button>\n  </form>\n</div>\n");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_auth_passwordchangesuccess_passwordchangesuccess_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"\n<div class=\"col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3\">\n  <form id=\"loginform\" class=\"form-signin\" role=\"form\" can-submit=\"done\"",can.view.pending({attrs: ['can-submit'], scope: scope,options: options}),">");___v1ew.push(
"\n    <h2>Password Change Successful</h2>\n    <p ");___v1ew.push(
can.view.txt(2,'p','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"font-size:18px;\"");return ___v1ew.join('')}));
___v1ew.push(
">Please <strong>delete the confirmation email</strong>. It has expired for your security.</p>\n    <a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'home')));___v1ew.push(
"\" class=\"btn btn-lg btn-success btn-block\" type=\"submit\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Log me in</a>\n  </form>\n</div>\n");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_auth_passwordemail_passwordemail_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div class=\"col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3\">\n  <h2>Reset Password</h2>\n  <p>Enter your email address to receive the password reset code.</p>\n  <form id=\"loginform\" class=\"form-signin\" role=\"form\" can-submit=\"send\"",can.view.pending({attrs: ['can-submit'], scope: scope,options: options}),">");___v1ew.push(
"\n    <input type=\"text\" class=\"form-control\" placeholder=\"Email Address\" required autofocus=\"\" can-value=\"email\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),"/>");___v1ew.push(
"\n    <div class=\"signup-messages bg-danger\">");___v1ew.push(
"\n");___v1ew.push(
can.view.txt(
0,
'div',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"nonexistent"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"        <p>That account does not exist.</p>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"    </div>\n    <br/>\n    <button class=\"btn btn-lg btn-success btn-block\" type=\"submit\">Reset Password</button>\n    <a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'login')));___v1ew.push(
"\" class=\"btn btn-block btn-link cancel\" type=\"button\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Already signed up? Login</a>\n  </form>\n</div>");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_auth_signup_signup_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div class=\"col-md-6 col-md-offset-3\">\n  <form id=\"signupform\" class=\"form-signin\" role=\"form\">\n  <h2>Create an Account</h2>\n  <div id=\"signup-email-error-message\"> </div>\n    <p>Enter &amp; confirm your email address.</p>\n    <div>\n      <input can-value=\"user.email\" type=\"text\" class=\"form-control top\" placeholder=\"email\" required=\"\" autofocus=\"\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),">");___v1ew.push(
"\n      <input can-value=\"user.email2\" type=\"text\" class=\"form-control bottom\" placeholder=\"email again\" required=\"\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),">");___v1ew.push(
"\n      <div class=\"signup-messages ");___v1ew.push(
can.view.txt(
true,
'div',
'class',
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"emailMismatch"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"bg-danger");return ___v1ew.join("");}}])));___v1ew.push(
" ");___v1ew.push(
can.view.txt(
true,
'div',
'class',
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"emailTaken"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"bg-danger");return ___v1ew.join("");}}])));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"\n");___v1ew.push(
can.view.txt(
0,
'div',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"emailMismatch"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"          <p>Email addresses do not match.</p>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"        ");___v1ew.push(
can.view.txt(
0,
'div',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"emailTaken"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"\n          <p>That email address is already in use.</p>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"      </div>\n    </div>\n    <br/>\n    <p>Enter &amp; confirm your password.</p>\n    <div>\n      <input can-value=\"user.password\" type=\"password\" class=\"form-control top\" placeholder=\"password\" required=\"\" minlength=\"6\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),">");___v1ew.push(
"\n      <input can-value=\"user.password2\" type=\"password\" class=\"form-control bottom\" placeholder=\"password again\" required=\"\" minlength=\"6\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),">");___v1ew.push(
"\n      <div class=\"signup-messages bg-danger\">");___v1ew.push(
"\n");___v1ew.push(
can.view.txt(
0,
'div',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"passwordMismatch"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"          <p>Passwords do not match.</p>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"      </div>\n    </div>\n    <br/>\n    <button class=\"btn btn-lg btn-success btn-block\" can-click=\"signup\"",can.view.pending({attrs: ['can-click'], scope: scope,options: options}),">");___v1ew.push(
"Create Account</button>\n    <a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'login')));___v1ew.push(
"\" class=\"btn btn-block btn-link cancel\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Already signed up? Login</a>\n  </form>\n</div>");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_auth_verify_verify_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div class=\"col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3\">\n  <h2>Verify Your Account</h2>\n  <p>Please enter the code from the verification email we sent you.</p>\n  <form id=\"loginform\" class=\"form-signin\" role=\"form\" can-submit=\"verify\"",can.view.pending({attrs: ['can-submit'], scope: scope,options: options}),">");___v1ew.push(
"\n    <input type=\"text\" class=\"form-control\" placeholder=\"Secret Code\" required autofocus=\"\" can-value=\"secret\"",can.view.pending({attrs: ['can-value'], scope: scope,options: options}),"/>");___v1ew.push(
"\n    <div class=\"signup-messages bg-danger\">");___v1ew.push(
"\n");___v1ew.push(
can.view.txt(
0,
'div',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"notVerified"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"        <p>That account has not been verified.  Please check your email and click the verification link.</p>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"    </div>\n    <br/>\n    <button class=\"btn btn-lg btn-success btn-block\" type=\"submit\">Verify Code</button>\n    <a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'login')));___v1ew.push(
"\" class=\"btn btn-block btn-link cancel\" type=\"button\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Already signed up? Login</a>\n  </form>\n</div>\n");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_pages_about_about_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div class=\"title blue\">\n\t<div class=\"container\">\n\t\t<h1>About Us</h1>\n\t</div>\n</div>\n<div id=\"about\">\n\t<div class=\"container\">\n\t\t<div id=\"menu_tablet\">\n\t\t\t<select id=\"menu_tab\">\n\t\t\t\t<option value=\"aboutus.htm\" selected",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"About us</option>\n\t\t\t\t<option value=\"team.htm\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Our team</option>\n\t\t\t\t<option value=\"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Our clients</option>\n\t\t\t\t<option value=\"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Press</option>\n\t\t\t\t<option value=\"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Jobs &amp; careers</option>\n\t\t\t\t<option value=\"contact.htm\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Contact</option>\n\t\t\t\t<option value=\"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Policy</option>\n\t\t\t</select>\n\t\t</div>\n\t\t<div class=\"content\">\n\t\t\t<div class=\"entry\">\n\t\t\t\t<h2>For a person of my generation, it pretty much goes without saying that <a href=\"#\">Robert De Niro</a> is the finest screen actor of his. </h2>\n\t\t\t\t<div class=\"image\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/pic_c_1.png\"");return ___v1ew.join('')}));
___v1ew.push(
" alt=\"\" /></div>\n\t\t\t\t<p>\n\t\t\t\t\tTo be a movie-besotted adolescent in the ’70s and early ’80s was\n\t\t\t\t\tto experience, in real time and at an impressionable age, performances\n\t\t\t\t\tthat would go on to become icons and monuments. ‘‘This kid doesn’t just\n\t\t\t\t\tact — he takes off into the vapors,’’ wrote Pauline Kael in her review\n\t\t\t\t\tof ‘‘<a href=\"#\">Mean Streets</a>.’’ Not that there was anything airy or\n\t\t\t\t\tabstract about what he was doing, which was transforming himself —\n\t\t\t\t\tphysically, vocally, psychologically — with each new role. And in the\n\t\t\t\t\tprocess, before our eyes, reinventing the art of acting.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\tI confess, however, that it took all my professional discipline\n\t\t\t\t\tto resist squandering the time I spent with De Niro on a recent Saturday\n\t\t\t\t\tafternoon in a slack-jawed fanboy recitation of his greatest hits. Oh,\n\t\t\t\t\tmy God, you’re Jake LaMotta! You’re Johnny Boy! You’re Travis Bickle! <a href=\"#\">I’m talking to you</a>.</p>\n\t\t\t\t\t<blockquote>\n\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\tYou forget your dreams, ignore your family, suppress your\n\t\t\t\t\t\t\tfeelings, neglect your friends, and forget to be happy. Errors of\n\t\t\t\t\t\t\tomission are a particularly dangerous type of mistake, because you make\n\t\t\t\t\t\t\tthem by default.\n\t\t\t\t\t\t</p>\n\t\t\t\t\t</blockquote>\n\t\t\t\t<p>\n\t\t\t\t\tTo the younger generation, though, he is most recognizably Jack\n\t\t\t\t\tByrnes, Ben Stiller’s impossible father-in-law in the ‘‘Fockers’’\n\t\t\t\t\tfranchise. And as the reliable heavy in a steady stream of action movies\n\t\t\t\t\tand crime dramas, some (but not all) of them quite good. It has become\n\t\t\t\t\tfashionable to suggest that De Niro’s best work is behind him. But\n\t\t\t\t\tnostalgia is a vice, and a survey of the last four decades of movie\n\t\t\t\t\thistory reveals that De Niro has never slackened, diminished or gone\n\t\t\t\t\taway but has rather, year in and year out, amassed a body of work marked\n\t\t\t\t\tby a seriousness and attention to detail that was there from the start.\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\t<div id=\"questions_tablet\">\n\t\t\t<div class=\"container\">\n\t\t\t\t<div class=\"content\">\n\t\t\t\t\t<p class=\"got_questions\">Got any questions? <a href=\"#\">Visit our help center</a> or <a href=\"#\">contact us</a></p>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t<div class=\"container\">\n\t\t<div id=\"sidebar\">\n\t\t\t<div class=\"menu\">\n\t\t\t\t<ul>\n\t\t\t\t\t<li class=\"current\"><a href=\"#\">About us</a></li>\n\t\t\t\t\t<li><a href=\"team.htm\">Our team</a></li>\n\t\t\t\t\t<li><a href=\"#\">Our clients</a></li>\n\t\t\t\t\t<li><a href=\"#\">Press</a></li>\n\t\t\t\t\t<li><a href=\"#\">Jobs &amp; careers</a></li>\n\t\t\t\t\t<li><a href=\"contact.htm\">Contact</a></li>\n\t\t\t\t\t<li><a href=\"#\">Policy</a></li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t<div class=\"cnt\">\n\t\t\t\t<h3>Twitter feed</h3>\n\t\t\t\t<ul>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<p>Our house is a building site. Kitchen floor coming up tomorrow. Whole family basically living in a couple of rooms.</p>\n\t\t\t\t\t\t<span>Dec 25</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<p>This Bellawood Flooring TV ad is really well done. Clear, direct, and hits all the right notes: <a href=\"http://t.co/TGBzpgw4\" class=\"link\">http://t.co/TGBzpgw4</a></p>\n\t\t\t\t\t\t<span>Dec 25</span>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<p>Whole family basically living in a couple of rooms.</p>\n\t\t\t\t\t\t<span>Dec 25</span>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t\t<div class=\"button\"><a href=\"#\" class=\"btn btn-blue btn-twitter\"><span></span>Follow Us</a></div>\n\t\t\t</div>\n\t\t\t<div class=\"form\">\n\t\t\t\t<h3>Invite Friends</h3>\n\t\t\t\t<form action=\"#\">\n\t\t\t\t\t<input class=\"email\" placeholder=\"mail@example.com\" type=\"text\">\n\t\t\t\t\t<input class=\"btn btn-blue\" value=\"Invite\" type=\"submit\">\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<div id=\"questions\">\n\t<div class=\"container\">\n\t\t<div class=\"content\">\n\t\t\t<p class=\"got_questions\">Got any questions? <a href=\"#\">Visit our help center</a> or <a href=\"#\">contact us</a></p>\n\t\t</div>\n\t</div>\n</div>\n");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_pages_blog_blog_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div class=\"title blue\">\n\t<div class=\"container\">\n\t\t<h1>Blog</h1>\n\t</div>\n</div>\n<div id=\"blog\">\n\t<div class=\"container\">\n\t\t<article class=\"content\">\n\t\t\t<div class=\"post\">\n\t\t\t\t<h2><a href=\"blog-single.htm\">Clarity of Expression</a></h2>\n\t\t\t\t<div class=\"meta_mob\">\n\t\t\t\t\t<p class=\"author\"><a href=\"#\">Alex Baldini</a> on 14 Jan 2013</p>\n\t\t\t\t\t<p class=\"comment\">15 comments</p>\n\t\t\t\t\t<p class=\"links\">Permalink</p>\n\t\t\t\t</div>\n\t\t\t\t<p>Breathing can transform your life.</p>\n\t\t\t\t<p>If you feel stressed out and overwhelmed, breathe. It will calm you and release the tensions.</p>\n\t\t\t\t<p>If you are worried about something coming up, or caught up in\n\t\t\t\t\tsomething that already happened, breathe. It will bring you back to the\n\t\t\t\t\tpresent.<br>If you are discouraged and have forgotten your purpose in\n\t\t\t\t\tlife, breathe. It will remind you about how precious life is, and that\n\t\t\t\t\teach breath in this life is a gift you need to appreciate. Make the most\n\t\t\t\t\tof this gift.\n\t\t\t\t</p>\n\t\t\t\t<nav class=\"buttons_mob\">\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">science</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">breathe</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">precious life</a>\n\t\t\t\t</nav>\n\t\t\t\t<p class=\"more-link\">\n\t\t\t\t\t<a href=\"blog-single.htm\">Read more...</a>\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<aside>\n\t\t\t\t<div class=\"meta\">\n\t\t\t\t\t<p class=\"author\"><a href=\"#\">Alex Baldini</a> on 14 Jan 2013</p>\n\t\t\t\t\t<p class=\"comment\">15 comments</p>\n\t\t\t\t\t<p class=\"links\">Permalink</p>\n\t\t\t\t</div>\n\t\t\t\t<nav class=\"buttons\">\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">science</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">breathe</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">precious life</a>\n\t\t\t\t</nav>\n\t\t\t</aside>\n\t\t</article>\n\t\t<article class=\"content\">\n\t\t\t<div class=\"post\">\n\t\t\t\t<h2><a href=\"blog-single.htm\">How to Get Startup Ideas</a></h2>\n\t\t\t\t<p><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic8.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" class=\"blog_img\" /></p>\n\t\t\t\t<p>The way to get startup ideas is not to try to think of startup ideas. It's to look for problems, preferably problems you have yourself. </p>\n\t\t\t\t<p>The very best startup ideas tend to have three things in common: they're something the founders themselves want, that they themselves can build,\n\t\t\t\t\tand that few others realize are worth doing. Microsoft, Apple, Yahoo, Google, and Facebook all began this way.\n\t\t\t\t</p>\n\t\t\t\t<nav class=\"buttons_mob\">\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">science</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">breathe</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">precious life</a>\n\t\t\t\t</nav>\n\t\t\t\t<p class=\"more-link\">\n\t\t\t\t\t<a href=\"blog-single.htm\">Read more...</a>\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<aside>\n\t\t\t\t<div class=\"meta\">\n\t\t\t\t\t<p class=\"author\"><a href=\"#\">Alex Baldini</a> on 14 Jan 2013</p>\n\t\t\t\t\t<p class=\"comment\">15 comments</p>\n\t\t\t\t\t<p class=\"links\">Permalink</p>\n\t\t\t\t</div>\n\t\t\t\t<nav class=\"buttons\">\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">science</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">breathe</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">precious life</a>\n\t\t\t\t</nav>\n\t\t\t</aside>\n\t\t</article>\n\t\t<article class=\"content\">\n\t\t\t<div class=\"post\">\n\t\t\t\t<h2><a href=\"blog-single.htm\">Frighteningly Ambitious Startup Ideas</a></h2>\n\t\t\t\t<p><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic10.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" class=\"blog_img\" /></p>\n\t\t\t\t<p>One of the more surprising things I've noticed while working on Y Combinator is how frightening the most ambitious startup ideas are.\n\t\t\t\t\tIn this essay I'm going to demonstrate this phenomenon by describing some. Any one of them could make you a billionaire.\n\t\t\t\t\tThat might sound like an attractive prospect, and yet when I describe these ideas you may notice you find yourself shrinking away from them.\n\t\t\t\t</p>\n\t\t\t\t<nav class=\"buttons_mob\">\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">science</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">breathe</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">precious life</a>\n\t\t\t\t</nav>\n\t\t\t\t<p class=\"more-link\">\n\t\t\t\t\t<a href=\"blog-single.htm\">Read more...</a>\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<aside>\n\t\t\t\t<div class=\"meta\">\n\t\t\t\t\t<p class=\"author\"><a href=\"#\">Alex Baldini</a> on 14 Jan 2013</p>\n\t\t\t\t\t<p class=\"comment\">15 comments</p>\n\t\t\t\t\t<p class=\"links\">Permalink</p>\n\t\t\t\t</div>\n\t\t\t\t<nav class=\"buttons\">\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">science</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">breathe</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">precious life</a>\n\t\t\t\t</nav>\n\t\t\t</aside>\n\t\t</article>\n\t\t<article class=\"content\">\n\t\t\t<div class=\"post\">\n\t\t\t\t<h2><a href=\"blog-single.htm\">Clarity of Expression</a></h2>\n\t\t\t\t<p>Breathing can transform your life.</p>\n\t\t\t\t<p>If you feel stressed out and overwhelmed, breathe. It will calm you and release the tensions.</p>\n\t\t\t\t<p>If you are worried about something coming up, or caught up in\n\t\t\t\t\tsomething that already happened, breathe. It will bring you back to the\n\t\t\t\t\tpresent.<br>If you are discouraged and have forgotten your purpose in\n\t\t\t\t\tlife, breathe. It will remind you about how precious life is, and that\n\t\t\t\t\teach breath in this life is a gift you need to appreciate. Make the most\n\t\t\t\t\tof this gift.\n\t\t\t\t</p>\n\t\t\t\t<nav class=\"buttons_mob\">\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">science</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">breathe</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">precious life</a>\n\t\t\t\t</nav>\n\t\t\t\t<p class=\"more-link\">\n\t\t\t\t\t<a href=\"blog-single.htm\">Read more...</a>\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<aside>\n\t\t\t\t<div class=\"meta\">\n\t\t\t\t\t<p class=\"author\"><a href=\"#\">Alex Baldini</a> on 14 Jan 2013</p>\n\t\t\t\t\t<p class=\"comment\">15 comments</p>\n\t\t\t\t\t<p class=\"links\">Permalink</p>\n\t\t\t\t</div>\n\t\t\t\t<nav class=\"buttons\">\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">science</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">breathe</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">precious life</a>\n\t\t\t\t</nav>\n\t\t\t</aside>\n\t\t</article>\n\t\t<article class=\"content\">\n\t\t\t<div class=\"post\">\n\t\t\t\t<h2><a href=\"blog-single.htm\">What We Look for in Founders</a></h2>\n\t\t\t\t<p>This has turned out to be the most important quality in startup founders. We thought when we started Y Combinator that the most important\n\t\t\t\t\tquality would be intelligence. That's the myth in the Valley. And certainly you don't want founders to be stupid. But as long as you're over a\n\t\t\t\t\tcertain threshold of intelligence, what matters most is determination. You're going to hit a lot of obstacles. You can't be the sort of person who gets\n\t\t\t\t\tdemoralized easily.\n\t\t\t\t</p>\n\t\t\t\t<p>Bill Clerico and Rich Aberman of WePay are a good example. They're doing a finance startup, which means endless negotiations with big, bureaucratic companies.\n\t\t\t\t\tWhen you're starting a startup that depends on deals with big companies to exist, it often feels like they're trying to ignore you out of existence. But when Bill Clerico\n\t\t\t\t\tstarts calling you, you may as well do what he asks, because he is not going away.\n\t\t\t\t</p>\n\t\t\t\t<nav class=\"buttons_mob\">\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">science</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">breathe</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">precious life</a>\n\t\t\t\t</nav>\n\t\t\t\t<p class=\"more-link\">\n\t\t\t\t\t<a href=\"blog-single.htm\">Read more...</a>\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<aside>\n\t\t\t\t<div class=\"meta\">\n\t\t\t\t\t<p class=\"author\"><a href=\"#\">Alex Baldini</a> on 14 Jan 2013</p>\n\t\t\t\t\t<p class=\"comment\">15 comments</p>\n\t\t\t\t\t<p class=\"links\">Permalink</p>\n\t\t\t\t</div>\n\t\t\t\t<nav class=\"buttons\">\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">science</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">breathe</a>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-silver\">precious life</a>\n\t\t\t\t</nav>\n\t\t\t</aside>\n\t\t</article>\n\t\t<div class=\"older_post\">\n\t\t\t<a href=\"#\" class=\"btn btn-red\">OLDER POSTS</a>\n\t\t</div>\n\t</div>\n</div>\n");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_pages_contact_contact_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div class=\"title green\">\n\t<div class=\"container\">\n\t\t<h1>Reach out and say hello!</h1>\n\t</div>\n</div>\n<div id=\"contact\">\n\t<div class=\"container\">\n\t\t<div id=\"menu_tablet\">\n\t\t\t<select id=\"menu_tab\">\n\t\t\t\t<option value=\"aboutus.htm\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"About us</option>\n\t\t\t\t<option value=\"team.htm\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Our team</option>\n\t\t\t\t<option value=\"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Our clients</option>\n\t\t\t\t<option value=\"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Press</option>\n\t\t\t\t<option value=\"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Jobs &amp; careers</option>\n\t\t\t\t<option value=\"contact.htm\" selected",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Contact</option>\n\t\t\t\t<option value=\"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Policy</option>\n\t\t\t</select>\n\t\t</div>\n\t\t<div class=\"content\">\n\t\t\t<div class=\"entry\">\n\t\t\t\t<div class=\"image\">\n\t\t\t\t\t<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d96875.06052141947!2d-74.36939135023778!3d40.644310669347995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4485efb0c050d75f!2z0JLQsNGI0LjQvdCz0YLQvtC9INCg0L7Qug!5e0!3m2!1sru!2s!4v1392018727151\" width=\"845\" height=\"623\" frameborder=\"0\" ");___v1ew.push(
can.view.txt(2,'iframe','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"border:0\"");return ___v1ew.join('')}));
___v1ew.push(
"></iframe>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"form\">\n\t\t\t\t<h3>Contact form</h3>\n\t\t\t\t<form method=\"post\" action=\"\">\n\t\t\t\t\t<textarea name=\"message\" rows=\"10\" cols=\"55\" placeholder=\"Message\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"</textarea>\n\t\t\t\t\t<input type=\"text\" name=\"email\" class=\"email\" placeholder=\"Email\">\n\t\t\t\t\t<input type=\"text\" name=\"obam\" class=\"obam\" placeholder=\"Obam\">\n\t\t\t\t\t<input type=\"submit\" value=\"Send\" class=\"btn btn-red\">\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t\t<div class=\"social\">\n\t\t\t\t<h3>Twitter feed</h3>\n\t\t\t\t<ul>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<div><a href=\"#\" class=\"twitter-ico\">twitter</a></div>\n\t\t\t\t\t\t<span>Dec 25</span>\n\t\t\t\t\t\t<p>Our house is a building site. Literally. Kitchen floor coming up tomorrow. Whole family basically living in a couple of rooms.</p>\n\t\t\t\t\t\t<em>Dec 25</em>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<div><a href=\"#\" class=\"twitter-ico\">twitter</a></div>\n\t\t\t\t\t\t<span>Dec 24</span>\n\t\t\t\t\t\t<p>This Bellawood Flooring TV ad is really well done. Clear, direct, and hits all the right notes: <br/><a href=\"http://t.co/TGBzpgw4\" class=\"link\">http://t.co/TGBzpgw4</a></p>\n\t\t\t\t\t\t<em>Dec 25</em>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<div><a href=\"#\" class=\"twitter-ico\">twitter</a></div>\n\t\t\t\t\t\t<span>Dec 20</span>\n\t\t\t\t\t\t<p>A real treat: A 14-minute design and driving review of the ultra-rare Aston Martin One-77: <br/><a href=\"http://t.co/TyBzpgwQ\" class=\"link\">http://t.co/TyBzpgwQ</a></p>\n\t\t\t\t\t\t<em>Dec 25</em>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t\t<div class=\"button\"><a href=\"#\" class=\"btn btn-blue btn-twitter\"><span></span>Follow Us</a></div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div id=\"sidebar\">\n\t\t\t<div class=\"menu\">\n\t\t\t\t<ul>\n\t\t\t\t\t<li><a href=\"aboutus.htm\">About us</a></li>\n\t\t\t\t\t<li><a href=\"team.htm\">Our team</a></li>\n\t\t\t\t\t<li><a href=\"#\">Our clients</a></li>\n\t\t\t\t\t<li><a href=\"#\">Press</a></li>\n\t\t\t\t\t<li><a href=\"#\">Jobs &amp; careers</a></li>\n\t\t\t\t\t<li class=\"currentb\"><a href=\"#\">Contact</a></li>\n\t\t\t\t\t<li><a href=\"#\">Policy</a></li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_pages_features_features_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div class=\"title red\">\n\t<div class=\"container\">\n\t\t<h1>Features</h1>\n\t</div>\n</div>\n<div id=\"sticker\">\n\t<div class=\"container\">\n\t\t<ul>\n\t\t\t<li><a href=\"#\" class=\"current\">Overview</a></li>\n\t\t\t<li><a href=\"#\">Video</a></li>\n\t\t\t<li><a href=\"#\">Perfect painting</a></li>\n\t\t\t<li><a href=\"#\">Future</a></li>\n\t\t\t<li><a href=\"#\">Crowded market</a></li>\n\t\t\t<li><a href=\"#\">This you need</a></li>\n\t\t\t<li><a href=\"#\">Start</a></li>\n\t\t</ul>\n\t\t<span>\n\t\t\tThis one sticks while scrolling down\n\t\t</span>\n\t\t<nav id=\"sticker-nav\">\n\t\t\t<select name=\"stciker-nav-mob\">\n\t\t\t\t<option value=\"1\" class=\"selected\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Overview</option>\n\t\t\t\t<option value=\"2\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Perfect Painting</option>\n\t\t\t\t<option value=\"3\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Future</option>\n\t\t\t\t<option value=\"4\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Crowded market</option>\n\t\t\t\t<option value=\"5\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Things you need</option>\n\t\t\t\t<option value=\"6\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Start</option>\n\t\t\t</select>\n\t\t</nav>\n\t</div>\n</div>\n<div id=\"startup\">\n\t<div class=\"container\">\n\t\t<h3>How to Get Startup Ideas</h3>\n\t\t<p>\n\t\t\tThe very best startup ideas tend to have three things in common: they're something the founders themselves want,\n\t\t\tthat they themselves can build, and that few others realize are worth doing.\n\t\t</p>\n\t\t<div>\n\t\t\t<a href=\"#\" class=\"btn-big btn-blue\"><span></span>Just Do It button</a>\n\t\t</div>\n\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/ipad.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"ipad\" />\n\t</div>\n</div>\n<div id=\"video\">\n\t<div class=\"container\">\n\t\t<div class=\"content\">\n\t\t\t<iframe src=\"http://www.youtube.com/embed/5HbYScltf1c\" frameborder=\"0\" allowfullscreen></iframe>\n\t\t</div>\n\t</div>\n</div>\n<div id=\"painting\">\n\t<div class=\"container\">\n\t\t<div class=\"iphone\">\n\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/iphone.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"iphone\" />\n\t\t</div>\n\t\t<div class=\"content\">\n\t\t\t<h1>You want to know how to paint a perfect painting?</h1>\n\t\t\t<p>It's easy. Make yourself perfect and then just paint naturally.</p>\n\t\t\t<div>\n\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/video.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"video\" />\n\t\t\t\t<h3>Problems</h3>\n\t\t\t\t<span>Why is it so important to work on a problem you have? Among other things, it ensures the problem really exists.</span>\n\t\t\t</div>\n\t\t\t<div>\n\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/bell.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"bell\" />\n\t\t\t\t<h3>Well</h3>\n\t\t\t\t<span>When a startup launches, there have to be at least some users who really need what they're making, who want it urgently.</span>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<div id=\"future\">\n\t<div class=\"container\">\n\t\t<div class=\"content\">\n\t\t\t<h1>Live in the future and build what seems interesting.</h1>\n\t\t\t<p>When something is described as a toy, that means it has everything an idea needs except being important.</p>\n\t\t\t<div>\n\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/video.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"video\" />\n\t\t\t\t<h3>It's cool</h3>\n\t\t\t\t<span>But if you're living in the future and you build something cool that users love, it may matter more than outsid-ers think.</span>\n\t\t\t</div>\n\t\t\t<div>\n\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/bell.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"bell\" />\n\t\t\t\t<h3>Users love it</h3>\n\t\t\t\t<span>Microcomputers seemed like toys when Apple and Microsoft started working on them.</span>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"future_slider\">\n\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/future_slider.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"slider\" />\n\t\t</div>\n\t</div>\n</div>\n<div id=\"gallery\">\n\t<div class=\"container\">\n\t\t<h1> \n\t\t\tA crowded market is actually a good sign\n\t\t</h1>\n\t\t<p class=\"text\">\n\t\t\tBecause it means both that there's demand and that none of the existing solutions are good enough.\n\t\t\tA startup can't hope to enter a market that's obviously big and yet in which they have no competitors.\n\t\t</p>\n\t\t<div id=\"gallery_tabs2\">\n\t\t\t<nav class=\"tabs\">\n\t\t\t\t\t<a href=\"#tabs-1\" title=\"America\" class=\"tabs_1\">America</a>\n\t\t\t\t\t<a href=\"#tabs-2\" title=\"Europe\" class=\"tabs_2\">Europe</a>\n\t\t\t\t\t<a href=\"#tabs-3\" title=\"Asia\" class=\"tabs_3\">Asia</a>\n\t\t\t</nav>\n\t\t\t<nav class=\"tabs_mob\">\n\t\t\t\t\t<a href=\"#tabs-1\" title=\"America\" class=\"tabs_1_mob\">America</a>\n\t\t\t\t\t<a href=\"#tabs-2\" title=\"Europe\" class=\"tabs_2_mob\">Europe</a>\n\t\t\t\t\t<a href=\"#tabs-3\" title=\"Asia\" class=\"tabs_3_mob\">Asia</a>\n\t\t\t</nav>\n\t\t\t<div class=\"tabs-content\">\n\t\t\t\t<div ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"display: block;\"");return ___v1ew.join('')}));
___v1ew.push(
" class=\"tab\">\n\t\t\t\t\t<div class=\"gallery_main_horz\">\n\t\t\t\t\t\t<div class=\"view1 view-first\">\n\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"images1\" href=\"home/images/gallery_pic1.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic1.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"You want to know how to paint a perfect painting?.\" height=\"630\" /></a>\n\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t<a href=\"#\" class=\"ico-zoom\"></a>\n\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"gallery_main_horz\">\n\t\t\t\t\t\t<div class=\"gallery_tabs_vert\">\n\t\t\t\t\t\t\t<div class=\"view2 view-first\">\n\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"images1\" href=\"home/images/gallery_pic2.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic2.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\"></a>\n\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"gallery_tabs_vert\">\n\t\t\t\t\t\t\t<div class=\"gallery_tabs_horz\">\n\t\t\t\t\t\t\t\t<div class=\"view3 view-first\">\n\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"images1\" href=\"home/images/gallery_pic3.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic3.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" width=\"270\" /></a>\n\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\"></a>\n\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"gallery_tabs_horz\">\n\t\t\t\t\t\t\t\t<div class=\"view3 view-first\">\n\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"images1\" href=\"home/images/gallery_pic4.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic4.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\"></a>\n\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"display: none;\"");return ___v1ew.join('')}));
___v1ew.push(
" class=\"tab\">\n\t\t\t\t\t<div class=\"gallery_main_horz\">\n\t\t\t\t\t\t<div class=\"view1 view-first\">\n\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"images\" href=\"home/images/gallery_pic5.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic5.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"You want to know how to paint a perfect painting?.\" height=\"630\" /></a>\n\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t<a href=\"#\" class=\"ico-zoom\"></a>\n\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"gallery_main_horz\">\n\t\t\t\t\t\t<div class=\"gallery_tabs_vert\">\n\t\t\t\t\t\t\t<div class=\"view2 view-first\">\n\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"images\" href=\"home/images/gallery_pic6.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic6.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\"></a>\n\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"gallery_tabs_vert\">\n\t\t\t\t\t\t\t<div class=\"gallery_tabs_horz\">\n\t\t\t\t\t\t\t\t<div class=\"view3 view-first\">\n\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"images\" href=\"home/images/gallery_pic7.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic7.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\"></a>\n\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"gallery_tabs_horz\">\n\t\t\t\t\t\t\t\t<div class=\"view3 view-first\">\n\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"images\" href=\"home/images/gallery_pic8.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic8.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\"></a>\n\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"display: none;\"");return ___v1ew.join('')}));
___v1ew.push(
" class=\"tab\">\n\t\t\t\t\t<div class=\"gallery_main_horz\">\n\t\t\t\t\t\t<div class=\"view1 view-first\">\n\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"images\" href=\"home/images/gallery_pic9.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic9.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"You want to know how to paint a perfect painting?.\" height=\"630\" /></a>\n\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t<a href=\"#\" class=\"ico-zoom\"></a>\n\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"gallery_main_horz\">\n\t\t\t\t\t\t<div class=\"gallery_tabs_vert\">\n\t\t\t\t\t\t\t<div class=\"view2 view-first\">\n\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"images\" href=\"home/images/gallery_pic10.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic10.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\"></a>\n\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"gallery_tabs_vert\">\n\t\t\t\t\t\t\t<div class=\"gallery_tabs_horz\">\n\t\t\t\t\t\t\t\t<div class=\"view3 view-first\">\n\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"images\" href=\"home/images/gallery_pic11.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic11.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\"></a>\n\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"gallery_tabs_horz\">\n\t\t\t\t\t\t\t\t<div class=\"view3 view-first\">\n\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"images\" href=\"home/images/gallery_pic12.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic12.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\"></a>\n\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"showimages\">\n\t\t\t<a href=\"gallery.htm\" class=\"btn-big btn-blue\"><span></span>Show more images</a>\n\t\t</div>\n\t</div>\n</div>\n<div id=\"macbook\" class=\"red\">\n\t<div class=\"container\">\n\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/macbook.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"Macbook\" />\n\t</div>\n</div>\n<div id=\"letsgo\" class=\"blue\">\n\t<div class=\"container\">\n\t\t<div class=\"content\">\n\t\t\t<h1>So, what do you say?</h1>\n\t\t\t<p>Live in the future and build what seems interesting. Strange as it sounds, that's the real recipe.</p>\n\t\t\t<div class=\"buttons\">\n\t\t\t\t<a href=\"#\" class=\"btn-do btn-white\"><span></span>Do it now</a>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_pages_home_home_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"\t<div id=\"slider\">\n\t\t<div class=\"fullwidthbanner-container\">\n\t\t\t<div class=\"fullwidthabnner\">\n\t\t\t\t<ul>\n\t\t\t\t\t<!-- BOXFADE -->\n\t\t\t\t\t<li data-transition=\"boxfade\" data-slotamount=\"6\" data-masterspeed=\"1000\">\n\t\t\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/slide1.png\"");return ___v1ew.join('')}));
___v1ew.push(
" />\n\t\t\t\t\t\t<div class=\"caption sfb big_white\" data-x=\"15\" data-y=\"105\" data-speed=\"900\" data-start=\"300\" data-easing=\"easeOutBack\">'A few strong instincts <br />and a few plain rules <br />suffice us.'</div>\n\t\t\t\t\t\t<div class=\"caption sfb small_text\" data-x=\"15\" data-y=\"270\" data-speed=\"900\" data-start=\"300\" data-easing=\"easeOutBack\">~Ralph Waldo Emerson</div>\n\t\t\t\t\t\t<div class=\"caption sfb\" data-x=\"15\" data-y=\"320\" data-speed=\"900\" data-start=\"300\" data-easing=\"easeOutBack\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/line.png\"");return ___v1ew.join('')}));
___v1ew.push(
" width=\"400\" height=\"1\" /></div>\n\t\t\t\t\t\t<div class=\"caption sfb\" data-x=\"450\" data-y=\"115\" data-speed=\"900\" data-start=\"300\" data-easing=\"easeOutBack\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/slide_image1.png\"");return ___v1ew.join('')}));
___v1ew.push(
" width=\"500\" height=\"288\" /></div>\n\t\t\t\t\t\t<div class=\"caption sfb\" data-x=\"15\" data-y=\"370\" data-speed=\"900\" data-start=\"300\" data-easing=\"easeOutBack\"><a href=\"#\" target=\"_blank\" class=\"btn btn-red\">Free trial</a></div>\n\t\t\t\t\t\t<div class=\"caption sfb\" data-x=\"155\" data-y=\"370\" data-speed=\"900\" data-start=\"300\" data-easing=\"easeOutBack\"><a href=\"#\" target=\"_blank\" class=\"btn btn-trans-white\">Learn more</a></div>\n\t\t\t\t\t</li>\n\t\t\t\t\t<!-- SLIDE LEFT -->\n\t\t\t\t\t<li data-transition=\"fade\" data-slotamount=\"6\" data-masterspeed=\"1000\">\n\t\t\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/slide2.png\"");return ___v1ew.join('')}));
___v1ew.push(
" />\n                            <div class=\"caption lfr very_large_text\" data-x=\"15\" data-y=\"140\" data-speed=\"900\" data-start=\"500\" data-easing=\"easeOutExpo\">A brief guide to life</div>\n\t\t\t\t\t\t<div class=\"caption lfr medium_text\" data-x=\"15\" data-y=\"230\" data-speed=\"900\" data-start=\"500\" data-easing=\"easeOutExpo\">Life can be ridiculously complicated,  <br />\n\t\t\t\t\t\t\tif you let it.</div>\n                            <div class=\"caption lfr\" data-x=\"15\" data-y=\"360\" data-speed=\"900\" data-start=\"500\" data-easing=\"easeOutExpo\"><a href=\"#\" target=\"_blank\" class=\"btn btn-red\">Go Slowly</a></div>\n\t\t\t\t\t</li>\n\t\t\t\t\t<!-- SLOTFADE HORIZONTAL -->\n\t\t\t\t\t<li data-transition=\"slotfade-horizontal\" data-slotamount=\"20\" data-masterspeed=\"1000\">\n\t\t\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/slide3.png\"");return ___v1ew.join('')}));
___v1ew.push(
" />\n\t\t\t\t\t\t<div class=\"caption lft large_text\" data-x=\"420\" data-y=\"90\" data-speed=\"1200\" data-start=\"800\" data-easing=\"easeOutBack\">'I never found the\n\t\t\t\t\t\t\t<br />companion that was <br />so companionable as <br />solitude. ’</div>\n\t\t\t\t\t\t<div class=\"caption lft small_text\" data-x=\"430\" data-y=\"320\" data-speed=\"1200\" data-start=\"800\" data-easing=\"easeOutBack\">~Henry David Thoreau</div>\n\t\t\t\t\t\t<div class=\"caption lft\" data-x=\"420\" data-y=\"360\" data-speed=\"1200\" data-start=\"800\" data-easing=\"easeOutBack\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/line.png\"");return ___v1ew.join('')}));
___v1ew.push(
" width=\"400\" height=\"1\" /></div>\n                            <div class=\"caption lft\" data-x=\"420\" data-y=\"400\" data-speed=\"1200\" data-start=\"800\" data-easing=\"easeOutBack\"><a href=\"#\" target=\"_blank\" class=\"btn btn-trans-white\">Learn more</a></div>\n\t\t\t\t\t</li>\n\t\t\t\t\t<!-- SLIDE  -->\n\t\t\t\t\t<li data-transition=\"slidehorizontal\" data-slotamount=\"7\" data-masterspeed=\"1000\"  data-fstransition=\"fade\" data-fsmasterspeed=\"1000\" data-fsslotamount=\"7\">\n\t\t\t\t\t\t\t\t<!-- MAIN IMAGE -->\n\t\t\t\t\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/video_forest.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
"  alt=\"video_forest\"  data-bgposition=\"center center\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\">\n\t\t\t\t\t\t\t\t<!-- LAYERS -->\n\t\t\t\t\t\t\t\t<!-- LAYER NR. 1 -->\n\t\t\t\t\t\t\t\t<div class=\"tp-caption tp-fade fadeout fullscreenvideo\"\n\t\t\t\t\t\t\t\t\tdata-x=\"0\"\n\t\t\t\t\t\t\t\t\tdata-y=\"0\"\n\t\t\t\t\t\t\t\t\tdata-speed=\"1000\"\n\t\t\t\t\t\t\t\t\tdata-start=\"1100\"\n\t\t\t\t\t\t\t\t\tdata-easing=\"Power4.easeOut\"\n\t\t\t\t\t\t\t\t\tdata-endspeed=\"1500\"\n\t\t\t\t\t\t\t\t\tdata-endeasing=\"Power4.easeIn\"\n\t\t\t\t\t\t\t\t\tdata-autoplay=\"true\"\n\t\t\t\t\t\t\t\t\tdata-autoplayonlyfirsttime=\"false\"\n\t\t\t\t\t\t\t\t\tdata-nextslideatend=\"true\"\n\t\t\t\t\t\t\t\t\tdata-forceCover=\"1\"\n\t\t\t\t\t\t\t\t\tdata-dottedoverlay=\"twoxtwo\"\n\t\t\t\t\t\t\t\t\tdata-aspectratio=\"16:9\"\n\t\t\t\t\t\t\t\t\tdata-forcerewind=\"on\"\n\t\t\t\t\t\t\t\t\t");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"z-index: 2\"");return ___v1ew.join('')}));
___v1ew.push(
">\n\t\t\t\t\t\t\t\t\t <video class=\"video-js vjs-default-skin\" preload=\"none\" width=\"100%\" height=\"100%\"\n\t\t\t\t\t\t\t\t\tposter='home/images/video_forest.jpg' data-setup=\"{}\">\n\t\t\t\t\t\t\t\t\t<source src='http://goodwebtheme.com/previewvideo/forest_edit.mp4' type='video/mp4' />\n\t\t\t\t\t\t\t\t\t<source src='http://goodwebtheme.com/previewvideo/forest_edit.webm' type='video/webm' />\n\t\t\t\t\t\t\t\t\t<source src='http://goodwebtheme.com/previewvideo/forest_edit.ogv' type='video/ogg' />\n\t\t\t\t\t\t\t\t\t</video>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"caption lft large_text\" data-x=\"420\" data-y=\"90\" data-speed=\"1200\" data-start=\"800\" data-easing=\"easeOutBack\" ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"z-index: 9\"");return ___v1ew.join('')}));
___v1ew.push(
">'I never found the\n\t\t\t\t\t\t\t\t\t<br />companion that was <br />so companionable as <br />solitude. ’</div>\n\t\t\t\t\t\t\t\t<div class=\"caption lft small_text\" data-x=\"430\" data-y=\"320\" data-speed=\"1200\" data-start=\"800\" data-easing=\"easeOutBack\" ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"z-index: 9\"");return ___v1ew.join('')}));
___v1ew.push(
">~Henry David Thoreau</div>\n\t\t\t\t\t\t\t\t<div class=\"caption lft\" data-x=\"420\" data-y=\"360\" data-speed=\"1200\" data-start=\"800\" data-easing=\"easeOutBack\" ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"z-index: 9\"");return ___v1ew.join('')}));
___v1ew.push(
"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/line.png\"");return ___v1ew.join('')}));
___v1ew.push(
" width=\"400\" height=\"1\" /></div>\n\t\t\t\t\t\t\t\t<div class=\"caption lft\" data-x=\"420\" data-y=\"400\" data-speed=\"1200\" data-start=\"800\" data-easing=\"easeOutBack\" ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"z-index: 9\"");return ___v1ew.join('')}));
___v1ew.push(
"><a href=\"#\" target=\"_blank\" class=\"btn btn-trans-white\">Learn more</a></div>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div id=\"painting\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"iphone\">\n\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/iphone.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"iphone\" />\n\t\t\t</div>\n\t\t\t<div class=\"content\">\n\t\t\t\t<h1>You want to know how to paint a perfect painting?</h1>\n\t\t\t\t<p>It's easy. Make yourself perfect and then just paint naturally.</p>\n\t\t\t\t<div class=\"first\">\n\t\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/video.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"video\" />\n\t\t\t\t\t<h3>Problems</h3>\n\t\t\t\t\t<span>Why is it so important to work on a problem you have? Among other things, it ensures the problem really exists.</span>\n\t\t\t\t</div>\n\t\t\t\t<div>\n\t\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/bell.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"bell\" />\n\t\t\t\t\t<h3>Well</h3>\n\t\t\t\t\t<span>When a startup launches, there have to be at least some users who really need what they're making, who want it urgently.</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div id=\"future\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"content\">\n\t\t\t\t<h1>Live in the future and build what seems interesting.</h1>\n\t\t\t\t<p>When something is described as a toy, that means it has everything an idea needs except being important.</p>\n\t\t\t\t<div class=\"first\">\n\t\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/video.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"video\" />\n\t\t\t\t\t<h3>It's cool</h3>\n\t\t\t\t\t<span>But if you're living in the future and you build something cool that users love, it may matter more than outsid-ers think.</span>\n\t\t\t\t</div>\n\t\t\t\t<div>\n\t\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/bell.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"bell\" />\n\t\t\t\t\t<h3>Users love it</h3>\n\t\t\t\t\t<span>Microcomputers seemed like toys when Apple and Microsoft started working on them.</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"future_slider\">\n\t\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/future_slider.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"slider\" />\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div id=\"gallery\">\n\t\t<div class=\"container\">\n\t\t\t<h1> \n\t\t\t\tA crowded market is actually a good sign\n\t\t\t</h1>\n\t\t\t<p class=\"text\">\n\t\t\t\tBecause it means both that there's demand and that none of the existing solutions are good enough.\n\t\t\t\tA startup can't hope to enter a market that's obviously big and yet in which they have no competitors.\n\t\t\t</p>\n\t\t\t<div id=\"gallery_tabs1\">\n\t\t\t\t<nav class=\"tabs\">\n\t\t\t\t\t\t<a href=\"#\" title=\"America\" class=\"tabs_1\">America</a>\n\t\t\t\t\t\t<a href=\"#\" title=\"Europe\" class=\"tabs_2\">Europe</a>\n\t\t\t\t\t\t<a href=\"#\" title=\"Asia\" class=\"tabs_3\">Asia</a>\n\t\t\t\t</nav>\n\t\t\t\t<nav class=\"tabs_mob\">\n\t\t\t\t\t\t<a href=\"#\" title=\"America\" class=\"tabs_1_mob\">America</a>\n\t\t\t\t\t\t<a href=\"#\" title=\"Europe\" class=\"tabs_2_mob\">Europe</a>\n\t\t\t\t\t\t<a href=\"#\" title=\"Asia\" class=\"tabs_3_mob\">Asia</a>\n\t\t\t\t</nav>\n\t\t\t\t<div class=\"tabs-content\">\n\t\t\t\t\t<div ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"display: block;\"");return ___v1ew.join('')}));
___v1ew.push(
" class=\"tab\">\n\t\t\t\t\t\t<div class=\"gallery_main_horz\">\n\t\t\t\t\t\t\t<div class=\"view1 view-first\">\n\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"1\" href=\"home/images/gallery_pic1.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic1.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"You want to know how to paint a perfect painting?.\" height=\"630\" /></a>\n\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"ico-zoom\" rel=\"1\"></a>\n\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"gallery_main_horz\">\n\t\t\t\t\t\t\t<div class=\"gallery_tabs_vert\">\n\t\t\t\t\t\t\t\t<div class=\"view2 view-first\">\n\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"1\" href=\"home/images/gallery_pic2.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic2.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"ico-zoom\" rel=\"1\"></a>\n\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"gallery_tabs_vert\">\n\t\t\t\t\t\t\t\t<div class=\"gallery_tabs_horz\">\n\t\t\t\t\t\t\t\t\t<div class=\"view3 view-first\">\n\t\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"1\" href=\"home/images/gallery_pic3.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic3.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" width=\"270\" /></a>\n\t\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\" rel=\"1\"></a>\n\t\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"gallery_tabs_horz\">\n\t\t\t\t\t\t\t\t\t<div class=\"view3 view-first\">\n\t\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"1\" href=\"home/images/gallery_pic4.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic4.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\" rel=\"1\"></a>\n\t\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"display: none;\"");return ___v1ew.join('')}));
___v1ew.push(
" class=\"tab\">\n\t\t\t\t\t\t<div class=\"gallery_main_horz\">\n\t\t\t\t\t\t\t<div class=\"view1 view-first\">\n\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"2\" href=\"home/images/gallery_pic5.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic5.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"You want to know how to paint a perfect painting?.\" height=\"630\" /></a>\n\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"ico-zoom\" rel=\"2\"></a>\n\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"gallery_main_horz\">\n\t\t\t\t\t\t\t<div class=\"gallery_tabs_vert\">\n\t\t\t\t\t\t\t\t<div class=\"view2 view-first\">\n\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"2\" href=\"home/images/gallery_pic6.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic6.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\" rel=\"2\"></a>\n\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"gallery_tabs_vert\">\n\t\t\t\t\t\t\t\t<div class=\"gallery_tabs_horz\">\n\t\t\t\t\t\t\t\t\t<div class=\"view3 view-first\">\n\t\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"2\" href=\"home/images/gallery_pic7.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic7.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\" rel=\"2\"></a>\n\t\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"gallery_tabs_horz\">\n\t\t\t\t\t\t\t\t\t<div class=\"view3 view-first\">\n\t\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"2\" href=\"home/images/gallery_pic8.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic8.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\" rel=\"2\"></a>\n\t\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"display: none;\"");return ___v1ew.join('')}));
___v1ew.push(
" class=\"tab\">\n\t\t\t\t\t\t<div class=\"gallery_main_horz\">\n\t\t\t\t\t\t\t<div class=\"view1 view-first\">\n\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"3\" href=\"home/images/gallery_pic9.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic9.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"You want to know how to paint a perfect painting?.\" height=\"630\" /></a>\n\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"ico-zoom\" rel=\"3\"></a>\n\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"gallery_main_horz\">\n\t\t\t\t\t\t\t<div class=\"gallery_tabs_vert\">\n\t\t\t\t\t\t\t\t<div class=\"view2 view-first\">\n\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"3\" href=\"home/images/gallery_pic10.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic10.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\" rel=\"3\"></a>\n\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"gallery_tabs_vert\">\n\t\t\t\t\t\t\t\t<div class=\"gallery_tabs_horz\">\n\t\t\t\t\t\t\t\t\t<div class=\"view3 view-first\">\n\t\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"3\" href=\"home/images/gallery_pic11.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic11.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\" rel=\"3\"></a>\n\t\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"gallery_tabs_horz\">\n\t\t\t\t\t\t\t\t\t<div class=\"view3 view-first\">\n\t\t\t\t\t\t\t\t\t\t<a class=\"lightbox\" rel=\"3\" href=\"home/images/gallery_pic12.jpg\" title=\"You want to know how to paint a perfect painting?.\" alt=\"It's easy. Make yourself perfect and then just paint naturally.\"><img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/gallery_pic12.jpg\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"\" /></a>\n\t\t\t\t\t\t\t\t\t\t<div class=\"mask\">\n\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" title=\"\" class=\"ico-zoom\" rel=\"3\"></a>\n\t\t\t\t\t\t\t\t\t\t\t<p>You want to know how to paint a perfect painting?.</p>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"showimages\">\n\t\t\t\t<a href=\"gallery.htm\" class=\"btn-big btn-blue\"><span></span>Show more images</a>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div id=\"macbook\">\n\t\t<div class=\"container\">\n\t\t\t<img ");___v1ew.push(
can.view.txt(2,'img','src',this,function(){var ___v1ew = [];___v1ew.push(
"src=\"");___v1ew.push(
"home/images/macbook.png\"");return ___v1ew.join('')}));
___v1ew.push(
" title=\"Macbook\" />\n\t\t</div>\n\t</div>\n\t<div id=\"letsgo\" class=\"red\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"content\">\n\t\t\t\t<h1>So, what do you say?</h1>\n\t\t\t\t<p>Live in the future and build what seems interesting. Strange as it sounds, that's the real recipe.</p>\n\t\t\t\t<div class=\"buttons\">\n\t\t\t\t\t<a href=\"#\" class=\"btn-lets btn-white\">Lets go</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_pages_pricing_pricing_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div class=\"title blue\">\n\t<div class=\"container\">\n\t\t<h1>Prices & Plans</h1>\n\t</div>\n</div>\n<div id=\"plan1\">\n\t<div class=\"container\">\n\t\t<div class=\"header\">\n\t\t\t<div class=\"header-content\">\n\t\t\t\t<p>Free for 30 days.</p>\n\t\t\t\t<span>After your trial ends, pick a plan that best fits your needs.</span>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"price-table\">\n\t\t\t<div class=\"header-content\">\n\t\t\t\t<div class=\"content\">\n\t\t\t\t\t<div class=\"block\">\n\t\t\t\t\t\t<header>\n\t\t\t\t\t\t\t<h3>Economy</h3>\n\t\t\t\t\t\t\t<h5>$9<span>/month</span></h5>\n\t\t\t\t\t\t</header>\n\t\t\t\t\t\t<div class=\"block-cnt\">\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><strong>10</strong> Small Cake</li>\n\t\t\t\t\t\t\t\t<li><strong>50</strong> Cups of Coffee</li>\n\t\t\t\t\t\t\t\t<li><strong>20</strong> Apples</li>\n\t\t\t\t\t\t\t\t<li><strong>Some</strong> Sugar</li>\n\t\t\t\t\t\t\t\t<li><strong>Unlimitted</strong> Milk</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-black\">Choose plan</a>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"best-block\">\n\t\t\t\t\t\t<header>\n\t\t\t\t\t\t\t<h3>Deluxe</h3>\n\t\t\t\t\t\t\t<h5>$15<span>/month</span></h5>\n\t\t\t\t\t\t</header>\n\t\t\t\t\t\t<div class=\"block-cnt\">\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><strong>70</strong> Small Cake</li>\n\t\t\t\t\t\t\t\t<li><strong>80</strong> Cups of Coffee</li>\n\t\t\t\t\t\t\t\t<li><strong>50</strong> Apples</li>\n\t\t\t\t\t\t\t\t<li><strong>Some</strong> more Sugar</li>\n\t\t\t\t\t\t\t\t<li><strong>Unlimitted</strong> Milk</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-black\">Choose plan</a>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"block\">\n\t\t\t\t\t\t<header>\n\t\t\t\t\t\t\t<h3>Business</h3>\n\t\t\t\t\t\t\t<h5>$30<span>/month</span></h5>\n\t\t\t\t\t\t</header>\n\t\t\t\t\t\t<div class=\"block-cnt\">\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><strong>500</strong> Small Cake</li>\n\t\t\t\t\t\t\t\t<li><strong>1000</strong> Cups of Coffee</li>\n\t\t\t\t\t\t\t\t<li><strong>700</strong> Apples</li>\n\t\t\t\t\t\t\t\t<li><strong>Unlimitted</strong> Sugar</li>\n\t\t\t\t\t\t\t\t<li><strong>Unlimitted</strong> Milk</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-black\">Choose plan</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<div id=\"plan2\">\n\t<div class=\"container\">\n\t\t<section class=\"header\">\n\t\t\t<div class=\"header-content\">\n\t\t\t\t<p>Free for 30 days.</p>\n\t\t\t\t<span>After your trial ends, pick a plan that best fits your needs.</span>\n\t\t\t</div>\n\t\t</section>\n\t\t<section class=\"price-table\">\n\t\t\t<div class=\"header-content\">\n\t\t\t\t<section class=\"content\">\n\t\t\t\t\t<div class=\"block\">\n\t\t\t\t\t\t<header>\n\t\t\t\t\t\t\t<h3>Economy</h3>\n\t\t\t\t\t\t\t<h5>$9<span>/month</span></h5>\n\t\t\t\t\t\t</header>\n\t\t\t\t\t\t<div class=\"block-cnt\">\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><strong>10</strong> Small Cake</li>\n\t\t\t\t\t\t\t\t<li><strong>50</strong> Cups of Coffee</li>\n\t\t\t\t\t\t\t\t<li><strong>20</strong> Apples</li>\n\t\t\t\t\t\t\t\t<li><strong>Some</strong> Sugar</li>\n\t\t\t\t\t\t\t\t<li><strong>Unlimitted</strong> Milk</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<a href=\"#\" class=\"btn\">Choose plan</a>\n\t\t\t\t\t\t<p>Free 30-day trial</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"best-block\">\n\t\t\t\t\t\t<header>\n\t\t\t\t\t\t\t<h3>Deluxe</h3>\n\t\t\t\t\t\t\t<h5>$15<span>/month</span></h5>\n\t\t\t\t\t\t</header>\n\t\t\t\t\t\t<div class=\"block-cnt\">\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><strong>70</strong> Small Cake</li>\n\t\t\t\t\t\t\t\t<li><strong>80</strong> Cups of Coffee</li>\n\t\t\t\t\t\t\t\t<li><strong>50</strong> Apples</li>\n\t\t\t\t\t\t\t\t<li><strong>Some</strong> more Sugar</li>\n\t\t\t\t\t\t\t\t<li><strong>Unlimitted</strong> Milk</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<a href=\"#\" class=\"btn\">Choose plan</a>\n\t\t\t\t\t\t<p>Best price. Free 30-day trial</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"block\">\n\t\t\t\t\t\t<header>\n\t\t\t\t\t\t\t<h3>Business</h3>\n\t\t\t\t\t\t\t<h5>$30<span>/month</span></h5>\n\t\t\t\t\t\t</header>\n\t\t\t\t\t\t<div class=\"block-cnt\">\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><strong>500</strong> Small Cake</li>\n\t\t\t\t\t\t\t\t<li><strong>1000</strong> Cups of Coffee</li>\n\t\t\t\t\t\t\t\t<li><strong>700</strong> Apples</li>\n\t\t\t\t\t\t\t\t<li><strong>Unlimitted</strong> Sugar</li>\n\t\t\t\t\t\t\t\t<li><strong>Unlimitted</strong> Milk</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<a href=\"#\" class=\"btn\">Choose plan</a>\n\t\t\t\t\t\t<p>Free 30-day trial</p>\n\t\t\t\t\t</div>\n\t\t\t\t</section>\n\t\t\t</div>\n\t\t</section>\n\t</div>\n</div>\n<div id=\"plan3\">\n\t<div class=\"container\">\n\t\t<section class=\"header\">\n\t\t\t<div class=\"header-content\">\n\t\t\t\t<p>Free for 30 days.</p>\n\t\t\t\t<span>After your trial ends, pick a plan that best fits your needs.</span>\n\t\t\t</div>\n\t\t</section>\n\t\t<section class=\"price-table\">\n\t\t\t<div class=\"header-content\">\n\t\t\t\t<section class=\"content\">\n\t\t\t\t\t<div class=\"block\">\n\t\t\t\t\t\t<header>\n\t\t\t\t\t\t\t<h3>Economy</h3>\n\t\t\t\t\t\t\t<h5>$9<span>/month</span></h5>\n\t\t\t\t\t\t</header>\n\t\t\t\t\t\t<div class=\"block-cnt\">\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><strong>10</strong> Small Cake</li>\n\t\t\t\t\t\t\t\t<li><strong>50</strong> Cups of Coffee</li>\n\t\t\t\t\t\t\t\t<li><strong>20</strong> Apples</li>\n\t\t\t\t\t\t\t\t<li><strong>Some</strong> Sugar</li>\n\t\t\t\t\t\t\t\t<li><strong>Unlimitted</strong> Milk</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-blue\">Choose plan</a>\n\t\t\t\t\t\t<p>Free 30-day trial</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"best-block\">\n\t\t\t\t\t\t<header>\n\t\t\t\t\t\t\t<h3>Deluxe</h3>\n\t\t\t\t\t\t\t<h5>$15<span>/month</span></h5>\n\t\t\t\t\t\t</header>\n\t\t\t\t\t\t<div class=\"block-cnt\">\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><strong>70</strong> Small Cake</li>\n\t\t\t\t\t\t\t\t<li><strong>80</strong> Cups of Coffee</li>\n\t\t\t\t\t\t\t\t<li><strong>50</strong> Apples</li>\n\t\t\t\t\t\t\t\t<li><strong>Some</strong> more Sugar</li>\n\t\t\t\t\t\t\t\t<li><strong>Unlimitted</strong> Milk</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<a href=\"#\" class=\"btn\">Choose plan</a>\n\t\t\t\t\t\t<p>Best price. Free 30-day trial</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"block\">\n\t\t\t\t\t\t<header>\n\t\t\t\t\t\t\t<h3>Business</h3>\n\t\t\t\t\t\t\t<h5>$30<span>/month</span></h5>\n\t\t\t\t\t\t</header>\n\t\t\t\t\t\t<div class=\"block-cnt\">\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><strong>500</strong> Small Cake</li>\n\t\t\t\t\t\t\t\t<li><strong>1000</strong> Cups of Coffee</li>\n\t\t\t\t\t\t\t\t<li><strong>700</strong> Apples</li>\n\t\t\t\t\t\t\t\t<li><strong>Unlimitted</strong> Sugar</li>\n\t\t\t\t\t\t\t\t<li><strong>Unlimitted</strong> Milk</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-blue\">Choose plan</a>\n\t\t\t\t\t\t<p>Free 30-day trial</p>\n\t\t\t\t\t</div>\n\t\t\t\t</section>\n\t\t\t</div>\n\t\t</section>\n\t</div>\n</div>\n");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_components_pages_styles_styles_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div class=\"title blue\">\n\t<div class=\"container\">\n\t\t<h1>Enjoy the show</h1>\n\t</div>\n</div>\n<div id=\"styles\">\n\t<div class=\"container\">\n\t\t<div class=\"block\">\n\t\t\t<h3><a href=\"\">buttons</a></h3>\n\t\t\t<p>\n\t\t\t\t<a href=\"#\" class=\"btn btn-blue\">Action</a>\n\t\t\t\t<a href=\"#\" class=\"btn btn-green\">Action</a>\n\t\t\t\t<a href=\"#\" class=\"btn btn-blue btn-twitter\"><span></span>Action</a>\n\t\t\t\t<a href=\"#\" class=\"btn btn-green btn-twitter\"><span></span>Action</a>\n\t\t\t\t<a href=\"#\" class=\"btn btn-red\">Action</a>\n\t\t\t\t<a href=\"#\" class=\"btn btn-trans-black\">Action</a>\n\t\t\t\t<a href=\"#\" class=\"btn btn-black\">Action</a>\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<a href=\"#\" class=\"btn-big btn-blue\"><span></span>Show more images</a>\n\t\t\t\t<a href=\"#\" class=\"btn-big btn-blue\"><span></span>Just Do It button</a>\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<a href=\"#\" class=\"btn-do btn-white\"><span></span>Do it now</a>\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<a href=\"#\" class=\"btn btn-social btn-fb\"><span></span>Connect</a>\n\t\t\t\t<a href=\"#\" class=\"btn btn-social btn-twitter\"><span></span>Connect</a>\n\t\t\t</p>\n\t\t</div>\n\t\t<div class=\"block\">\n\t\t\t<h3><a href=\"#\">links</a></h3>\n\t\t\t<p>\n\t\t\t\tAll links are blue and underlined on mouseover. Headers on Blog are dark but they are links. Hover effects shown on <span>07_blog.psd</span>\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<a href=\"#\" class=\"link\">Donec metus leo, elementum at ultrices ac, dapibus at justo. </a>\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<a href=\"#\" class=\"link\">Donec metus leo, elementum at ultrices ac, dapibus at justo. </a>\n\t\t\t</p>\n\t\t</div>\n\t\t<div class=\"block\">\n\t\t\t<h3><a href=\"#\">forms</a></h3>\n\t\t\t<p>\n\t\t\t\t<form method=\"post\" action=\"#\" class=\"form\">\n\t\t\t\t\t<input type=\"text\" name=\"email\" placeholder=\"Email\" class=\"email\">\n\t\t\t\t\t<input type=\"password\" name=\"email\" placeholder=\"Password\" class=\"password\">\n\t\t\t\t</form>\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<form method=\"post\" action=\"#\" class=\"form2\">\n\t\t\t\t\t<input type=\"text\" name=\"password\" placeholder=\"Email\" class=\"email\">\n\t\t\t\t\t<input type=\"password\" name=\"email\" placeholder=\"Password\" class=\"password\">\n\t\t\t\t</form>\n\t\t\t</p>\n\t\t</div>\n\t\t<div class=\"block\">\n\t\t\t<h3><a href=\"#\">tabs</a></h3>\n\t\t\t<div class=\"tabs\">\n\t\t\t\t<nav class=\"tabs-nav\">\n\t\t\t\t\t<a class=\"active\" href=\"#\">Tab 1</a>\n\t\t\t\t\t<a href=\"#\">Tab 2</a>\n\t\t\t\t\t<a href=\"#\">Tab 3</a>\n\t\t\t\t</nav>\n\t\t\t\t<div class=\"tabs-cnt\">\n\t\t\t\t\t<div ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"display: block;\"");return ___v1ew.join('')}));
___v1ew.push(
" class=\"tab\">\n\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis. Donec metus leo,\n\t\t\t\t\t\t\telementum at ultrices ac, dapibus at justo. Donec tristique hendrerit\n\t\t\t\t\t\t\tdui vitae lacinia. Suspendisse ante ligula, adipiscing porta aliquam et,\n\t\t\t\t\t\t\trutrum nec lectus. Nulla erat risus, molestie non dapibus ac, fermentum\n\t\t\t\t\t\t\tvitae felis\n\t\t\t\t\t\t</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"display: none;\"");return ___v1ew.join('')}));
___v1ew.push(
" class=\"tab\">\n\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t2 Donec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis. Donec metus leo,\n\t\t\t\t\t\t\telementum at ultrices ac, dapibus at justo. Donec tristique hendrerit\n\t\t\t\t\t\t\tdui vitae lacinia. Suspendisse ante ligula, adipiscing porta aliquam et,\n\t\t\t\t\t\t\trutrum nec lectus. Nulla erat risus, molestie non dapibus ac, fermentum\n\t\t\t\t\t\t\tvitae felis\n\t\t\t\t\t\t</p>\n\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t2 Donec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis. Donec metus leo,\n\t\t\t\t\t\t\telementum at ultrices ac, dapibus at justo. Donec tristique hendrerit\n\t\t\t\t\t\t\tdui vitae lacinia. Suspendisse ante ligula, adipiscing porta aliquam et,\n\t\t\t\t\t\t\trutrum nec lectus. Nulla erat risus, molestie non dapibus ac, fermentum\n\t\t\t\t\t\t\tvitae felis\n\t\t\t\t\t\t</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div ");___v1ew.push(
can.view.txt(2,'div','style',this,function(){var ___v1ew = [];___v1ew.push(
"style=\"");___v1ew.push(
"display: none;\"");return ___v1ew.join('')}));
___v1ew.push(
" class=\"tab\">\n\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t3 Donec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis. Donec metus leo,\n\t\t\t\t\t\t\telementum at ultrices ac, dapibus at justo. Donec tristique hendrerit\n\t\t\t\t\t\t\tdui vitae lacinia. Suspendisse ante ligula, adipiscing porta aliquam et,\n\t\t\t\t\t\t\trutrum nec lectus. Nulla erat risus, molestie non dapibus ac, fermentum\n\t\t\t\t\t\t\tvitae felis\n\t\t\t\t\t\t</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"block\">\n\t\t\t<h3><a href=\"#\">toggles</a></h3>\n\t\t\t<div class=\"accordion\">\n\t\t\t\t<ul>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<h6><span></span>Title 1</h6>\n\t\t\t\t\t\t<div class=\"acc-cnt\">\n\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis. Donec metus leo,\n\t\t\t\t\t\t\t\telementum at ultrices ac, dapibus at justo. Donec tristique hendrerit\n\t\t\t\t\t\t\t\tdui vitae lacinia. Suspendisse ante ligula, adipiscing porta aliquam et,\n\t\t\t\t\t\t\t\trutrum nec lectus. Nulla erat risus, molestie non dapibus ac, fermentum\n\t\t\t\t\t\t\t\tvitae felis\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<h6><span></span>Title 2</h6>\n\t\t\t\t\t\t<div class=\"acc-cnt\">\n\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis. Donec metus leo,\n\t\t\t\t\t\t\t\telementum at ultrices ac, dapibus at justo. Donec tristique hendrerit\n\t\t\t\t\t\t\t\tdui vitae lacinia. Suspendisse ante ligula, adipiscing porta aliquam et,\n\t\t\t\t\t\t\t\trutrum nec lectus. Nulla erat risus, molestie non dapibus ac, fermentum\n\t\t\t\t\t\t\t\tvitae felis\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<h6><span></span>Title 3</h6>\n\t\t\t\t\t\t<div class=\"acc-cnt\">\n\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis. Donec metus leo,\n\t\t\t\t\t\t\t\telementum at ultrices ac, dapibus at justo. Donec tristique hendrerit\n\t\t\t\t\t\t\t\tdui vitae lacinia. Suspendisse ante ligula, adipiscing porta aliquam et,\n\t\t\t\t\t\t\t\trutrum nec lectus. Nulla erat risus, molestie non dapibus ac, fermentum\n\t\t\t\t\t\t\t\tvitae felis\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"block\">\n\t\t\t<h3><a href=\"#\">alerts</a></h3>\n\t\t\t<div class=\"cols\">\n\t\t\t\t<a href=\"#\" class=\"alert alert-orange\">Yes, you did it!</a>\n\t\t\t\t<a href=\"#\" class=\"alert alert-blue\">Yes, you did it!</a>\n\t\t\t\t<a href=\"#\" class=\"alert alert-pink\">Yes, you did it!</a>\n\t\t\t</div>\n\t\t\t<div class=\"cols\">\n\t\t\t\t<a href=\"#\" class=\"alert alert-white\">Yes, you did it!</a>\n\t\t\t\t<a href=\"#\" class=\"alert alert-grey\">Yes, you did it!</a>\n\t\t\t\t<a href=\"#\" class=\"alert alert-green\">Yes, you did it!</a>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"block\">\n\t\t\t<h3><a href=\"#\">columns</a></h3>\n\t\t\t<div class=\"grid grid-2\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-2 last-col\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-3\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-3\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-3 last-col\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-4\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-4\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-4\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-4 last-col\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-6\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-6\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-6\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-6\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-6\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid grid-6 last-col\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"grid last-col\">\n\t\t\t\t<p>\n\t\t\t\t\tDonec metus leo, elementum at ultrices ac, dapibus at justo.\n\t\t\t\t\tDonec tristique hendrerit dui vitae lacinia. Suspendisse ante ligula,\n\t\t\t\t\tadipiscing porta aliquam et, rutrum nec lectus. Nulla erat risus,\n\t\t\t\t\tmolestie non dapibus ac, fermentum vitae felis\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_nav_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div id=\"header\">\n  <div class=\"container\">\n    <div id=\"logo\">\n      <a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'home')));___v1ew.push(
"\" title=\"My Money Anywhere\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"My Money Anywhere</a>\n      <span>money. smart.</span>\n    </div>\n    <nav id=\"menu\">\n      <ul class=\"sf-menu\">\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'features')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Features</a></li>\n        <li>\n          <a href=\"javascript:void(0);\">Works</a>\n          <ul>\n            <li><a href=\"gallery.htm\">Gallery</a></li>\n            <li><a href=\"team.htm\">Happy team</a></li>\n            <li><a href=\"javascript:void(0);\">Pages</a>\n              <ul>\n                <li><a href=\"index.htm#painting\">Painting</a></li>\n                <li><a href=\"index.htm#future\">Future</a></li>\n                <li><a href=\"index.htm#gallery\">Lightbox Slider</a></li>\n              </ul>\n            </li>\n            <li><a href=\"gallery-single.htm\">Single Portfolio</a></li>\n            <li><a href=\"javascript:void(0);\">Shortcodes</a>\n              <ul>\n                <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'typography')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Typography</a></li>\n                <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'shortcodes')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Shortcodes element</a></li>\n              </ul>\n            </li>\n            <li><a href=\"features.htm#video\">Movie</a></li>\n          </ul>\n        </li>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'about')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"About Us</a></li>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'pricing')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Pricing</a></li>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'styles')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Styles</a></li>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'blog')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Blog</a></li>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'contact')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Contact</a></li>");___v1ew.push(
"\n");___v1ew.push(
can.view.txt(
0,
'ul',
0,
this,
can.Mustache.txt(
{scope:scope,options:options},
"#",{get:"if"},{get:"isLoggedIn"},[

{fn:function(scope,options){var ___v1ew = [];___v1ew.push(
"        <li><a href=\"/dash\" class=\"btn-login\">My Dashboard</a></li>\n        ");return ___v1ew.join("");}},
{inverse:function(scope,options){
var ___v1ew = [];___v1ew.push(
"\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'login')));___v1ew.push(
"\" class=\"btn-login\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Login</a></li>");___v1ew.push(
"\n");return ___v1ew.join("");}}])));___v1ew.push(
"      </ul>\n    </nav>\n    <div class=\"menu-trigger\"></div>\n    <nav id=\"menu-mobile\">\n      <ul>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'features')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Features</a></li>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'works')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Works</a></li>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'about')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"About Us</a></li>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'pricing')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Pricing</a></li>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'styles')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Styles</a></li>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'blog')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Blog</a></li>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'contact')));___v1ew.push(
"\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Contact</a></li>\n        <li><a href=\"");___v1ew.push(
can.view.txt(
true,
'a',
'href',
this,
can.Mustache.txt(
{scope:scope,options:options},
null,{get:"hrefTo"},'login')));___v1ew.push(
"\" class=\"login\"",can.view.pending({scope: scope,options: options}),">");___v1ew.push(
"Login</a></li>\n      </ul>\n    </nav>\n  </div>\n</div>");; return ___v1ew.join('') }));
can.view.preloadStringRenderer('home_site_mustache',can.Mustache(function(scope,options) { var ___v1ew = [];___v1ew.push(
"<div id=\"wrap\">\n\t");___v1ew.push(
can.view.txt(
0,
'div',
0,
this,
function(){ return can.Mustache.renderPartial('home/nav.mustache',scope,options)}));
___v1ew.push(
"\n\t<div id=\"main\">Loading ...</div>\n\t<div id=\"footer\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"col\">\n\t\t\t\t<h5>Main</h5>\n\t\t\t\t<ul>\n\t\t\t\t\t<li><a href=\"#\" title=\"Start Here\">Start Here</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Portfolio\">Portfolio</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Meet US\">Meet US</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Blog\">Blog</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Contact\">Contact</a></li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t<div class=\"col\">\n\t\t\t\t<h5>Company</h5>\n\t\t\t\t<ul>\n\t\t\t\t\t<li><a href=\"#\" title=\"Start Here\">About</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Portfolio\">Help</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Meet US\">Support</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Blog\">Jobs</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Contact\">Directory</a></li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t<div class=\"col\">\n\t\t\t\t<h5>One More</h5>\n\t\t\t\t<ul>\n\t\t\t\t\t<li><a href=\"#\" title=\"Start Here\">Meetsup</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Portfolio\">Handbook</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Meet US\">Privacy</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Blog\">API</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Contact\">Equipment</a></li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t<div class=\"col\">\n\t\t\t\t<h5>One More</h5>\n\t\t\t\t<ul>\n\t\t\t\t\t<li><a href=\"#\" title=\"Start Here\">Meetsup</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Portfolio\">Handbook</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Meet US\">Privacy</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Blog\">API</a></li>\n\t\t\t\t\t<li><a href=\"#\" title=\"Contact\">Equipment</a></li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t<div class=\"right\">\n\t\t\t\t<div class=\"social\">\n\t\t\t\t\t<ul>\n\t\t\t\t\t\t<li><a href=\"#\" class=\"twitter-ico\">twitter</a></li>\n\t\t\t\t\t\t<li><a href=\"#\" class=\"facebook-ico\">facebook</a></li>\n\t\t\t\t\t\t<li><a href=\"#\" class=\"vimeo-ico\">vimeo</a></li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"copy\">\n\t\t\t\t\t<p>@2013 Endeavour </p>\n\t\t\t\t\t<p><a href=\"#\">Privacy Policy</a> <span></span> <a href=\"#\">Terms of Service</a></p>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"gotop\"><a href=\"#\"></a></div>\n</div>\n");; return ___v1ew.join('') })); 
})(this);