'use strict';

import './home.css!';

import Session from './models/session';

import './components/auth/auth';

import './components/pages/home/home';
import './components/pages/features/features';
import './components/pages/about/about';
import './components/pages/pricing/pricing';
import './components/pages/styles/styles';
import './components/pages/blog/blog';
import './components/pages/contact/contact';


// Everything revolves around the appState.
import appState from './appState';

// Routes
can.route('passwordemail/:email',{page: 'passwordemail'});
can.route('passwordchange/:secret',{page: 'passwordchange'});
can.route('verify/:secret',{page: 'verify'});
can.route(':page',{page: 'home'});


// Register Mustache Helper
can.mustache.registerHelper('linkTo', (page) => can.mustache.safeString(can.route.link(page,{page: page})) );
can.mustache.registerHelper('hrefTo', (page) => can.mustache.safeString(can.route.url({page: page})) );

$(document.body).append( can.view('home/site.mustache', appState) );

var pages = {
	login: '<sc-login session="{session}"></sc-login>'
};

appState.bind('showPage', (ev, newVal) => {
	if(newVal) {
		var template = pages[newVal] || '<sc-'+newVal+'></sc-'+newVal+'>';
		$('#main').html(  can.mustache( template )( appState ) );
	}
});

Session.findOne({}).then(
	// Successful token login
	(session) => {
		appState.attr({
			session: session,
			ready : true
		});
		can.route.ready();
	},
	// Failed token login.
	() => {
		appState.attr('ready', true);
		can.route.ready();
	});
