'use strict';

module.exports = can.Component.extend({
	tag: 'sc-styles',
	template: can.view('home/components/pages/styles/styles.mustache'),
	scope: {}
});