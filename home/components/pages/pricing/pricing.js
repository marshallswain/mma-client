'use strict';

module.exports = can.Component.extend({
	tag: 'sc-pricing',
	template: can.view('home/components/pages/pricing/pricing.mustache'),
	scope: {}
});