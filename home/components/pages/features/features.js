'use strict';

module.exports = can.Component.extend({
	tag: 'sc-features',
	template: can.view('home/components/pages/features/features.mustache'),
	scope: {}
});