'use strict';

module.exports = can.Component.extend({
	tag: 'sc-contact',
	template: can.view('home/components/pages/contact/contact.mustache'),
	scope: {}
});