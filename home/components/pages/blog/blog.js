'use strict';

module.exports = can.Component.extend({
	tag: 'sc-blog',
	template: can.view('home/components/pages/blog/blog.mustache'),
	scope: {}
});