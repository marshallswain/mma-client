'use strict';

module.exports = can.Component.extend({
	tag: 'sc-about',
	template: can.view('home/components/pages/about/about.mustache'),
	scope: {}
});