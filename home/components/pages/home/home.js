'use strict';

module.exports = can.Component.extend({
	tag: 'sc-home',
	template: can.view('home/components/pages/home/home.mustache'),
	scope: {}
});