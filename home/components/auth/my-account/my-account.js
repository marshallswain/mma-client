'use strict';

import '../change-password/change-password';

can.Component.extend({
	tag: 'sc-my-account',
	template: can.view('home/components/auth/my-account/my-account.mustache'),
	scope: {}
});