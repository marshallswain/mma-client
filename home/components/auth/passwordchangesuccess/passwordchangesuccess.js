'use strict';

can.Component.extend({
	tag: 'sc-passwordchangesuccess',
	template: can.view('home/components/auth/passwordchangesuccess/passwordchangesuccess.mustache'),
});
