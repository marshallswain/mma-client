'use strict';

export var Account = can.Feathers.Model.extend('Account', {
	resource: '/api/accounts'
}, {});

export var Preference = can.Feathers.Model.extend('Preference', {
	resource: '/api/preferences'
}, {});

export var Todo = can.Feathers.Model.extend('Todo', {
	resource: '/api/todos'
}, {});

export var Secret = can.Feathers.Model.extend('Secret', {
	resource: '/api/secrets'
}, {});

export var Ticket = can.Feathers.Model.extend('Ticket', {
	resource: '/api/tickets'
}, {});

export var User = can.Feathers.Model.extend('User', {
	resource: '/api/users'
}, {});

export var PublicUser = can.Model.extend('PublicUser', {
	resource: '/api/users'
}, {});

export var Future = can.Feathers.Model.extend('Future', {
	resource: '/api/future_transactions',
	attributes : {
		date : 'date'
	},
	convert:{
		date: function(raw){
			return new Date.create(raw);
		}
	}
}, {});

export var Reserved = can.Feathers.Model.extend('Reserved', {
	resource: '/api/reserved_transactions',
	attributes : {
		date : 'date'
	},
	convert:{
		date: function(raw){
			return new Date.create(raw);
		}
	}
}, {});

export var Transaction = can.Feathers.Model.extend('Transaction', {
	resource: '/api/transactions',
	attributes : {
		date : 'date'
	},
	convert:{
		date: function(raw){
			return new Date.create(raw);
		}
	}
}, {});

export var BankTransaction = can.Feathers.Model.extend('BankTransaction', {
	resource: '/api/bank_transactions',
	attributes : {
		date : 'date'
	},
	convert:{
		date: function(raw){
			return new Date.create(raw);
		}
	}
}, {});
