'use strict';

export var correspondingSidebarSections = {
  overview: "overview",
  todos: "overview",
  labels: "overview",
  creditscore: "overview",
  accounts:'accounts',
  account:'accounts',
  calendar:'calendar',
  budget:'budget',
  planning:'planning',
  contacts:'contacts',
  reports:'reports',
  settings:'settings'
};
