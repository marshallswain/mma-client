'use strict';

export var planningSections = {
  'protect':'Protect',
  'emergency-start':'Emergency Fund',
  'debt': 'Debt',
  'emergency-full' : 'Full Emergency Fund',
  'home':'Pay Off Home',
  'retirement':'Save for Retirement',
  'dreams': 'Other Goals & Dreams',
  'wealth':'Build Wealth'
};
