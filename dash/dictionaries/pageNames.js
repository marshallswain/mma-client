'use strict';

export var pageNames = {
  overview: "Overview",
  todos: "Todo List",
  labels: "Labels",
  creditscore: "Credit Score",
  accounts:'Accounts',
  account:'Accounts',
  calendar:'Calendar',
  budget:'Budget',
  planning:'Planning',
  contacts:'Contacts',
  reports:'Reports & Charts',
  settings:'Settings',
  buttons:'Settings : Buttons'
};
