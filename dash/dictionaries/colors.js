'use strict';

export var colors = {
  overview:'#00AABB',
  todos:'#00AABB',
  labels:'#00AABB',
  creditscore:'#00AABB',
  accounts:'#23ae89',
  account:'#23ae89',
  calendar:'#ffb61c',
  budget:'#f98e33',
  planning:'#e94b3b',
  contacts:'#449dd5',
  reports:'#1c7ebb',
  settings:'#6a55c2',
  buttons:'#6a55c2'
};
