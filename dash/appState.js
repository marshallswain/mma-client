'use strict';

import  {Todo} from './models';
import  {Account} from './models';
import  {Preference} from './models';
import  {Transaction} from './models';

import {colors} from './dictionaries/colors';

// Make the socket available to other parts of appState.
var socket;

// Defines the state of the application
var AppState = can.Map.extend({
	define : {
		todos: {serialize: false },

		accounts: {
			serialize: false,
			set(accts){
				// If an account_id is set, set up the account.
				// This makes sure an account is set up on page load,
				// otherwise the Account_ID set() wouldn't find an account.
				if(this.attr('account_id')){
					for (var i = 0; i < accts.length; i++) {
						if (accts[i]._id == this.attr('account_id')) {
							this.attr('account', accts[i]);
						}
					}
				}
				return accts;
			}
		},

		account_id:{
			serialize:true,
			set(id){
				this.attr('Account_ID', id);
				return id;
			}
		},

		Account_ID:{
			serialize:false,
			set(id){

				// If accounts are already loaded from the server, set up the account.
				var accts = this.attr('accounts');
				if(accts){
					for (var i = 0; i < accts.length; i++) {
						if (accts[i]._id == id) {
							this.attr('account', accts[i]);
						}
					}
				}



				return id;
			}
		},

		account:{
			serialize:false
		},

		acct_section:{
			serialize:true
		},

		Acct_Section:{
			serialize: false,
			get(){
				var section = this.attr('acct_section');
				switch (section) {
				case 'scheduled':
					return 'Scheduled';
				case 'tags':
					return 'Tags';
				case 'history':
					return 'History';
				}
			}
		},

		page: {
			set(value){
				return value;
			}
		},

		CurrentPage:{
			get(){
				var pages = this.attr('pages');
				return pages[this.attr('page')];
			}
		},

		pages: {
			serialize:false,
			value:{
				overview: "Overview",
				todos: "Todo List",
				labels: "Labels",
				creditscore: "Credit Score",
				accounts:'Accounts',
				account:'Accounts',
				calendar:'Calendar',
				budget:'Budget',
				planning:'Planning',
				contacts:'Contacts',
				reports:'Reports & Charts',
				settings:'Settings',
				buttons:'Settings : Buttons'
			}
		},

		correspondingSidebarSections: {
			serialize:false,
			value:{
				overview: "overview",
				todos: "overview",
				labels: "overview",
				creditscore: "overview",
				accounts:'accounts',
				account:'accounts',
				calendar:'calendar',
				budget:'budget',
				planning:'planning',
				contacts:'contacts',
				reports:'reports',
				settings:'settings'
			}
		},

		color:{
			serialize:false,
			value:''
		},

		colors: {
			serialize: false,
			value:colors
		},

		// Observed by the right sidebar.
		sidebar:{
			serialize:true
		},

		// The txn displayed in the right sidebar.
		txn:{serialize: false},

		session : {
			serialize: false,
			set() {
				var self = this;

				// When there is a session, start the websockets.
				socket = io('', {query: 'token=' + localStorage.getItem('featherstoken'), transports: ['websocket'] });
				can.Feathers.connect(socket);

				// Fetch the user's data
				this.attr('todos', new Todo.List({}));

				Preference.findOne({}, function(prefs){

					// Function to set prefs, once obtained or created.
					var setPrefs = function(prefObj){
						self.attr('prefs', prefObj);

						// Once prefs are in place, delegate on all changes.
						self.delegate('prefs', 'change', function(ev, attr, how, newVal, oldVal){
							// If there's an update, save a delta update to the server.
							if (newVal !== oldVal) {
								var pref = {
									_id: self.attr('prefs._id'),
									userID: self.attr('prefs.userID')
								};
								pref[attr] = newVal;
								socket.emit('api/preferences::update', pref._id, pref, {}, function() {});
							}
						});
					};

					// If there aren't any prefs, create the defaults on the server.
					if(!prefs){
						new Preference({}).save(function(preferences){
							setPrefs(preferences);
						});

					// If we have prefs, setPrefs.
					} else {
						setPrefs(prefs);
					}
				});

				Account.findAll({}, function(accounts){
					self.attr('accounts', accounts);
				});
			},
			remove() {
				// When the session is removed, stop routing.
				can.route.attr('page', 'home');

				// Remove data.
				this.removeAttr('todos');
				this.removeAttr('accounts');

				// can.route._teardown();
				localStorage.removeItem('featherstoken');
			}
		},

		isLoggedIn: {
			get() { return !!this.attr('session'); }
		},

		// Set to true once we know if a session has been established or not.
		ready: {
			serialize: false
		},

		prefs:{
			serialize:false,
			value:{
				Pivots:{
					Accounts: false,
	        Budget: false,
	        Calendar: false,
	        Contacts: false,
	        Overview: false,
	        Planning: false,
	        Reports: false,
	        Settings: false
				},
				DefaultSidebars:{
					overview: "overview",
	        todos: "todos",
	        labels: "labels",
	        creditscore: "creditscore",
	        accounts: "accounts",
	        account: "account",
	        calendar: "calendar",
	        budget: "budget",
	        planning: "planning",
	        contacts: "contacts",
	        reports: "reports",
	        settings: "settings"
				}
			}
		},

		showPage: {
			get() {
				if( this.attr('session') ) {
					return this.attr('page');
				} else {
					return 'login';
				}
			}
		}
	},

	logout() {
		this.removeAttr('session');
	}
});

var appState = new AppState();
export default appState;
window.appState = appState;

can.route.map(appState);
