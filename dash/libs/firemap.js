'use strict';

var FireMap = can.Map.extend({
	'setup':function(snapshot){
		console.log(snapshot);
		this.new = true;

		// Get the data from the snapshot.
		var data = snapshot.val();

		// Save the fb ref with it.
		this.fb = {
			// The ref to the data.
			ref:snapshot.ref()
		};

		this._super(data);
	},
	'init':function(){
		var self = this;

		// When an attribute changes, update firebase.
		this.bind('change', function(ev, attr, how, newVal, oldVal){
			var data = this.attr();
			delete data.fb;
			this.fb.ref.set(data);
		});

		// Listen for updates from Firebase.
		this.fb.ref.on('value', function(snapshot){
			if (!self.new) {
				var data = snapshot.val();
				self.attr(data);
				console.info('Applied update from Firebase');
			} else {
				self.new = false;
			}
		});
	}
});

FireMap.List = can.List.extend({
	Map:FireMap
}, {});

module.exports = FireMap;