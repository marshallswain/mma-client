'use strict';

import './dash.css!';
import './styles/panels.css!';
import './fonts.css!';

import Session from './models/session';

import './mustache-helpers';
import './routes';

/* * * Components * * */
import './components/auth/auth';

import './components/header/top-header/top-header';
import './components/header/bread-crumbs/bread-crumbs';
import './components/left-sidebar/left-sidebar';
import './components/pages/pages';
import './components/right-sidebar/right-sidebar';

import './components/utils/panel-pivot/panel-pivot';
import './components/utils/panel-accordian/panel-accordian';

/* * * Main Application State * * */
import './context/responsive'; // Controls how many sidebars are in view.

// Extending String to be able to capitalize easily. Only the first letter.
String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

// Everything revolves around the appState.
import appState from './appState';

$(document.body).append( can.view('/dash/site.stache', appState) );


Session.findOne({}).then(
	// Successful token login
	function(session){
		appState.attr({
			session: session,
			ready : true
		});
		can.route.ready();
	},
	// Failed token login.
	function(){
		appState.attr('ready', true);
		can.route.ready();
	});
