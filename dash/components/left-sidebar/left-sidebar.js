'use strict';

import './pivot/pivot';
import './left-accounts/left-accounts';

import {correspondingSidebarSections} from '../../dictionaries/correspondingSidebarSections';

import appState from '../../../appState';

can.Component.extend({
	tag: 'left-sidebar',
	template: can.view('/dash/components/left-sidebar/left-sidebar.stache'),
	scope: {
		appState:appState
	},
	helpers:{
		// Determine if the pref is open (true) or closed (false)
		pivotState(prefVal){
			if (prefVal()) {
				return 'open';
			} else {
				return 'closed';
			}
		},

		// Determine if the pref is "collapse in" (true) or "collapse" (false)
		collapseState(prefVal, options){
			// Get the sidebar menu collapsible container & run collapse() on it.
			var el = $(options.nodeList[0]);
			// Don't animate on load.
			if (!el.hasClass('loaded')) {
				el.addClass('loaded');
				// Show open ones immediately on load.
				if (prefVal()) {
					el.show();
				}
			// After done loading, animate any future changes.
			} else {
				// If prevVal is true, it should be open.
				if (prefVal()) {
					el.slideDown(200);
				} else {
					el.slideUp(200);
				}
			}
		},

		highlightActive(page){
			var correspondingPage = correspondingSidebarSections[appState.attr('page')];
			if (page == correspondingPage) {
				return 'active';
			}
		}
	}
});
