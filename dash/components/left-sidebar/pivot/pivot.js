'use strict';

import './pivot.css!';
import appState from '../../../appState';

can.Component.extend({
	tag:'pivot-handle',
	template:can.view('/dash/components/left-sidebar/pivot/pivot.stache'),
	scope:{
		define:{
			state:{
				value:'@',
				get(){
					var pref = appState.attr('prefs.Pivots.'+this.attr('pivot'));
					if (pref) {
						return 'open';
					} else {
						return 'closed';
					}
				}
			}
		},

		// Toggle the Pivot Pref's boolean when the pivot is clicked.
		toggle(scope){
			var value = appState.attr('prefs.Pivots.'+ scope.attr('pivot'));
			appState.attr('prefs.Pivots.'+ scope.attr('pivot'), !value);
		}
	},
	helpers:{
		pivotState(prefVal){
			if (prefVal()) {
				return 'open';
			} else {
				return 'closed';
			}
		}
	}
});
