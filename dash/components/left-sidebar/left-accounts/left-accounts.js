'use strict';

import appState from '../../../appState';

can.Component.extend({
  tag: 'left-sidebar-accounts',
  template: can.view('/dash/components/left-sidebar/left-accounts/left-accounts.stache'),
  scope: appState
});
