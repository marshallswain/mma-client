'use strict';

import appState from '../../../appState';
import {Preference} from '../../../models';

can.Component.extend({
	tag: 'sc-settings',
	template: can.view('/dash/components/pages/settings/settings.stache'),
	scope: {
		appState:appState,
		open(scope, el){
			var section = $(el).parent().attr('pref');
			appState.prefs.attr(section, true);
		},
		close(scope, el){
			var section = $(el).parent().attr('pref');
			appState.prefs.attr(section, false);
		}
	},
	events: {},
	helpers:{
		open(value){
			if(value()){
				return '';
			} else {
				return 'line-';
			}
		},
		closed(value){
			if(!value()){
				return '';
			} else {
				return 'line-';
			}
		}
	}
});
