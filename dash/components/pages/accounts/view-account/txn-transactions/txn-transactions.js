'use strict';

import appState from '../../../../../appState';

can.Component.extend({
	tag: 'txn-transactions',
  template: can.view('/dash/components/pages/accounts/view-account/txn-transactions/txn-transactions.stache'),

	scope: {

		// Passed in from view-account.js
		transactions:null,

		// Function to set appState.txn to the clicked txn.
		openTxn(scope, el, ev){

			// Set the txn to appState.
			var txn = el.data('txn');
			appState.attr('txn', txn);

			// Set the appState's txn_id
			appState.attr('txn_id', txn.id);

			// Select the history acct_section.
			if(appState.attr('acct_section') !== 'history'){
				appState.attr('acct_section', 'history');
			}

			// Prevent click from bubbling to the acct_section
			ev.stopPropagation();
		}

	},
	// events: {
	//
  //   'init':function(){
  //     this.count = 0;
	//
  //   },
	//
	// 	// When txnsloaded, push them into the scope.
	// 	'{appState} txnsloaded change':function(){
	// 		// this.scope.attr('txns', appState.txns[can.route.attr('account_id')]);
	//
	// 		// Now that we have txns, run activeRow().
	// 		// this.activeRow();
	// 	},
	//
  //   /**
  //    * If the txn belongs to the visible account, return true.
  //    * @param  {object} txn
  //    * @return {boolean} whether or not the txn belongs to the same account.
  //    */
  //   'sameAccount':function(txn){
  //     if (txn.accountID == appState.account.id) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   },
	//
	// 	/**
  //    * Sort txns on update.
  //    */
	// 	'{Transaction} updated':function(Transaction, ev, txn){
  //     // If we're dealing with the same account...
  //     if (this.sameAccount(txn)) {
	//
  // 			// Reset the ctxTxnIndex.
  // 			this.scope.ctxTxnIndex = this.scope.txns.indexOf(txn);
	//
  // 			// Set the ctxTxnIndex to the new location
  // 			this.sortOnUpdate(txn);
  //     }
	// 	},
	//
	// 	/**
  //    * When a new transaction is created, push it on the correct list.
  //    */
	// 	'{Transaction} created':function(Transaction, ev, txn){
  //     // If we're dealing with the same account...
  //     if (this.sameAccount(txn)) {
	//
  // 			// Set the ctxTxnIndex to the new location
  // 			this.scope.ctxTxnIndex = null;
	//
  // 			// Sort the txn into the list.
  //       this.sortedInsert(txn);
  //     }
	// 	},
	//
	// 	// Function to manage the .active row.
	// 	'activeRow':function(){
	// 		// Remove active class from all rows.
	// 		this.element.find('tr').removeClass('active');
	//
	// 		// Add active class to the currently-selected row.
	// 		this.element.find('[data-url="/'+ window.location.hash +'"]').addClass('active');
	// 	},
	//
	// 	// When the route changes, run activeRow().
	// 	'accounts/:account_id/history/:txn_id route':function(){
	// 		this.activeRow();
	// 	},
	//
	// 	// When a row is clicked, show the sidebar-txn-history
	// 	'tr click': function(el, ev){
	// 		ev.stopPropagation();
	// 		window.location.href = el.attr('data-url');
	// 	},
	//
	//
	// 	/**
	// 	 * Check prev & next txn dates to see if the txn needs to be moved.
	// 	 *
	// 	 * If it does need to move, it removes it from the list and returns the index
	// 	 * from where it was removed.
	// 	 */
	// 	'needsToMove': function(txn){
	// 		var self = this;
	//
	// 		var index = self.scope.ctxTxnIndex;
	//
	// 		// For keeping track of the txn. Innocent until proven guilty.
	// 		var correctBefore = true;
	// 		var correctAfter = true;
	//
	// 		// Check if previous txn is out of order.
	// 		if (self.scope.txns[index - 1]) {
	//
	// 			// It's in the wrong place if the txn date is higher.
	// 			if (txn.date >= self.scope.txns[index - 1].date) {
	// 				// console.log('previous txn out of order');
	// 				correctBefore = false;
	// 			}
	// 		}
	//
	// 		// Check if next txn is out of order.
	// 		if (self.scope.txns[index + 1]) {
	//
	// 			// console.warn(txn.description);
	// 			// console.log(txn.date);
	// 			// console.warn(self.scope.txns[index + 1].description);
	// 			// console.log(self.scope.txns[index + 1].date);
	//
	// 			// It's in the wrong place if the txn date is lower.
	// 			if (txn.date <= self.scope.txns[index + 1].date) {
	// 				// console.log('next txn out of order');
	// 				correctAfter = false;
	// 			}
	// 		}
	//
	// 		// If either one was out of order,
	// 		if (!(correctBefore && correctAfter)) {
	//
	// 			// console.log('needs to move');
	//
	// 			// Return true
	// 			return true;
	//
	// 		} else {
	//
	// 			// console.log('doesn\'t need to move');
	// 			// console.log('but we will pretend it did.')
	//
	// 			// Doesn't need to move.
	// 			return false;
	// 			// return true;
	// 		}
	// 	},
	//
	// 	/**
	// 	 * If the txn needs to move, figure out where it's supposed to go.
	// 	 */
	// 	'sortOnUpdate':function(txn){
	// 		var self = this;
	//
	// 		// If the txn needs to move...
	// 		if (self.needsToMove(txn)) {
	//
	// 			// Remove it
	// 			self.scope.txns.splice(self.scope.ctxTxnIndex, 1);
	//
	// 			self.sortedInsert(txn);
	//
	// 		// Otherwise, just update the balance and remove the lock
	// 		} else {
	//
  //       // Update date.
  //       self.setDate(txn);
	//
  //       // Update balance.
	// 			self.balanceAllFromIndex(self.scope.ctxTxnIndex);
	// 		}
	// 	},
	//
  //   /**
  //    * Test if a txn is a deposit
  //    * @param  {object} txn to test
  //    * @return {boolean}   true or false, it is or is not a deposit.
  //    */
  //   'isDeposit': function(txn){
  //     if (txn.amount > 0) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   },
	//
	//
  //   /**
  //    * Modifies the date of the passed object.  If a lastMatch index is passed, use it as
  //    *  a starting point and add one second.  If there's no match, add 12 hours to withdrawals
  //    *  so they appear after any deposits that may happen on the same day.
  //    *
  //    * @param  {object} txn to be modified.
  //    * @param  {integer} lastMatch index of last matching transaction, if one existed.
  //    * @return {[type]}     [description]
  //    */
  //   'setDate':function(txn, lastMatch){
	//
  //     // If a match was passed
  //     if (lastMatch !== undefined) {
	//
  //       // Get the date from the index at lastMatch
  //       var date = this.scope.txns[lastMatch].date;
	//
  //       // Add one second to the last match and set the time to the txn.date
  //       txn.attr('date',  new Date(date.getTime() + 1000));
	//
  //     // If no match was passed, and it's a deposit.
  //     } else if(this.isDeposit(txn)) {
	//
  //       // Set it up on 0000 seconds on the same date.
  //       txn.attr('date', new Date(txn.date.getFullYear(), txn.date.getMonth(), txn.date.getDate()));
	//
  //     // If no match was passed, and it's a withdrawal.
  //     } else {
	//
  //       // Get the date without time.
  //       var wDate = new Date(txn.date.getFullYear(), txn.date.getMonth(), txn.date.getDate());
	//
  //       // Add 12 hours. 1000 * 60 * 60 * 12
  //       txn.attr('date',  new Date(wDate.getTime() + 43200000));
  //     }
  //   },
	//
	//
	// 	/**
	// 	 * Sort a txn into the list.
	// 	 * Find where it's supposed to go.
	// 	 *   2a. Deposits go in first.
	// 	 *   2b. Withdrawals go out after deposits.
	// 	 */
	// 	'sortedInsert':function(txn){
	// 		var self = this;
	//
	// 		// For tracking if we've found where the txn should go.
	// 		var foundASpot = false;
	//
	// 		// The index the same day started.
	// 		var sameDayStartsAtIndex;
	//
	// 		// Get the first millisecond possible on the date for later comparison.
	// 		var dateFirst = new Date(txn.date.getFullYear(), txn.date.getMonth(), txn.date.getDate());
	//
	// 		// Get the last millisecond possible on the date for later comparison.
	// 		var dateLast = new Date(dateFirst.getTime() + 86399999);
	//
	// 		// Getting ready to store the last matching index.
	// 		var lastMatch = null;
	//
	// 		// Loop through txns to find possible same-day transactions.
	// 		self.scope.txns.each(function(transaction, index){
	//
	// 			// If the date is on the same day as the txn.
	// 			if (transaction.date >= dateFirst && transaction.date < dateLast) {
	//
	// 				// And the amounts are of the same sign (positive or negative)
	// 				if ((txn.amount * transaction.amount) > 0) {
	//
	// 					// Save it to lastMatch
	// 					lastMatch = index;
	// 				}
	//
	// 				// If this is the first matching day, store the index.
	// 				if (!sameDayStartsAtIndex) {
	// 					sameDayStartsAtIndex = index;
	// 				}
	//
	// 			}
	// 		});
	//
	// 		// If another transaction was found on the same date with the same sign...
	// 		// We don't care if it was a deposit or not at this point, since we can just add
	// 		// a second to the last txn and we'll be in the right spot.
	// 		if (lastMatch !== null) {
	//
  //       // Set the date.
	// 			self.setDate(txn, lastMatch);
	//
	// 			// Splice the txn into the list.
	// 			self.scope.txns.splice(lastMatch, 0, txn);
	//
	// 			// Start balancing at the newly-inserted index.
	// 			self.balanceAllFromIndex(lastMatch);
	//
	// 			// We found a spot.
	// 			foundASpot = true;
	//
	// 		} else {
	//
  //       // Set the date without a lastMatch.
  //       self.setDate(txn);
	//
  //       // If there's another txn on the same day
  //       if (sameDayStartsAtIndex !== null) {
	//
  //       }
	//
  // 			// There were no other txns on this same day, we need to place it chronologically.
  // 			// It will become the first txn on this day.
  // 			if (!foundASpot) {
	//
  // 				// We won't execute unnecessary logic if we've placed a txn.
  // 				var placed = false;
	//
  // 				// Loop through the txns...
  // 				self.scope.txns.each(function(transaction, index){
	//
  // 					if(!placed) {
	//
  // 						var first = self.scope.txns[0];
  // 						var next = self.scope.txns[index+1];
  // 						var last = self.scope.txns[self.scope.txns.length - 1];
	//
  // 						// If the index is zero, just compare the first item.
  // 						if (!index && (txn.date > first.date)) {
  // 							console.log('first');
	//
  // 							self.scope.txns.splice(0, 0, txn);
	//
	//
  // 							// Update balances starting at the current index.
  // 							self.balanceAllFromIndex(0);
	//
  // 							// Mark as placed.
  // 							placed = true;
	//
  // 						// If the txn date is between the current index and the next index
  // 						} else if((next && next.date) && (txn.date < transaction.date) && (txn.date > next.date)){
  // 							console.log('second');
  // 							// splice it in.
  // 							self.scope.txns.splice(index+1, 0, txn);
	//
  // 							// Update balances starting at the inserted index.
  // 							self.balanceAllFromIndex(index+1);
	//
  // 							// Mark as placed.
  // 							placed = true;
	//
  // 						// The txn date should go last. Push to the list.
  // 						} else if(txn.date < last.date) {
  // 							console.log('third');
	//
  // 							self.scope.txns.push(txn);
	//
  // 							// Update balances starting at the inserted index -1.
  // 							self.balanceAllFromIndex(self.scope.txns.length-1);
	//
  // 							// Mark as placed.
  // 							placed = true;
  // 						}
  // 					}
  // 				});
	//
  // 			}
  //     }
	//
	//
	// 		// TODO: Implement a "recursive search with a back-off" algorithm.
	// 		// Cut the list in half and check the half-way point.  If the half-way point
	// 		// has a matching date, go backwards until you find a non-matching date, then
	// 		// loop through the matching dates to get the next time stamp available on that
	// 		// same date.
	// 		// var half = Math.round(length / 2);
	//
	// 		// Check first and last to avoid looping through the entire list if the modified
	// 		// transaction's date falls outside the existing range in the list.
	// 		// var length = self.scope.txns.length;
	// 		// var first = self.scope.txns[0].date;
	// 		// var last = self.scope.txns[length-1].date;
	//
	// 	},
	//
	// 	'balanceChanged':function(index){
	// 		var self = this;
	//
	// 		// For keeping track of the differences in balance, if any. Innocent until proven guilty.
	// 		var correctBefore = true;
  //     var correctAfter = true;
	//
	// 		// Check if previous txn is out of order.
	// 		if (self.scope.txns[index - 1]) {
	//
	// 			// console.warn(index);
	// 			// console.warn(self.scope.txns[index].balance/100);
	// 			// console.warn(index + 1);
	// 			// console.warn(self.scope.txns[index + 1].balance/100);
	//
	// 			// The balance changed if the lower-index balance doesn't equal the current balance + the lower-index amount.
	// 			if (self.scope.txns[index - 1].balance !== self.scope.txns[index].balance + self.scope.txns[index -1].amount) {
	// 				// console.log('previous txn wrong balance');
	// 				correctBefore = false;
	// 			}
	// 		}
	//
	//
	// 		// If there is a txn at the next index, use it's balance.
	// 		if (self.scope.txns[index + 1]) {
	//
	// 			// The balance changed if the current balance doesn't equal the next balance + the current amount.
	// 			if (self.scope.txns[index].balance !== self.scope.txns[index + 1].balance + self.scope.txns[index].amount) {
	//
	// 				// console.log(self.scope.txns[index].balance);
	// 				// console.log(self.scope.txns[index + 1].balance);
	// 				// console.log(self.scope.txns[index].amount);
	// 				// console.log((self.scope.txns[index + 1].balance + self.scope.txns[index].amount));
	// 				// console.log('wrong balance based on next txn');
	//
	// 				correctAfter = false;
	// 			}
	//
	// 		// There was no txn at the next index, so use the account's balance.
	// 		} else {
	//
	// 			// console.warn(self.scope.txns[index].description);
	// 			// console.log('txn balance');
	// 			// console.warn(self.scope.txns[index]);
	// 			// console.log('txn amount');
	// 			// console.log(self.scope.txns[index].amount);
	// 			// console.log('plus account balance of');
	// 			// console.log(appState.account.attr('balance.beginning'));
	// 			// console.log('equals');
	// 			// console.log(appState.account.attr('balance.beginning') + self.scope.txns[index].amount);
	//
	// 			// The balance changed if the current balance doesn't equal the next balance + the current amount.
	// 			if (self.scope.txns[index].balance !== appState.account.attr('balance.beginning') + self.scope.txns[index].amount) {
	//
	// 				// console.log('wrong balance based on account.');
	//
	// 				correctAfter = false;
	// 			}
	//
	// 		}
	//
	// 		// If balances look good
	// 		if (correctBefore && correctAfter) {
	//
	// 			// console.log('Balance is good.');
	//
	// 			// Balance is good.
	// 			return false;
	//
  //     } else {
	//
  //       // console.log('Balance needs updating');
  //       // Balance needs updating.
  //       return true;
	//
	// 		}
	// 	},
	//
	// 	/**
	// 	 * Updates the balance for the transaction at the passed-in index
	// 	 * and all transactions that follow it (oldest to newest).  If an updatedIndex
	// 	 * is passed in, the highest index will be used since it represents the oldest txn.
	// 	 *
	// 	 * @param  {integer} index - The index of the starting transaction in scope.txns.
	// 	 * @param  {integer} updatedIndex - on updates, the index of the txn before it was
	// 	 *                                  removed from the list.
	// 	 */
	// 	'balanceAllFromIndex':function(index){
	// 		var self = this;
	//
	//
	//
	// 		// If the updatedIndex is higher (older txn), use it as the starting point.
	// 		// if(self.scope.ctxTxnIndex && self.scope.ctxTxnIndex > index){
  //  //      console.log('index is now ' + self.scope.ctxTxnIndex);
  //  //      index = self.scope.ctxTxnIndex;
	// 		// }
	//
	// 		if (self.balanceChanged(index)) {
	//
	// 			// console.log('Balancing all txns from index ' + index + ' down.');
	//
	//
	// 			/* * Iterate and update balances * */
  //       for (var i = index; i >= 0; i--) {
	//
  //         // Setting scope of balance here.
  //         var balance;
	//
  //         // If there's a higher index (not the oldest txn)...
  //         if (self.scope.txns[i + 1]) {
	//
  //           // Set the balance to the next oldest txn's balance.
  //           balance = self.scope.txns[i+1].balance;
	//
  //         // if there's no higher index (this is the oldest txn)...
  //         } else {
	//
  //           // start with the account's beginning balance.
  //           balance = appState.account.attr('balance.beginning');
  //         }
	//
  //         // Set to the determined balance.
  //         self.scope.txns[i].attr('balance', balance + self.scope.txns[i].attr('amount'));
	//
  //         if (self.count < 2) {
	//
  //           // console.log(self.scope.txns[i].attr('id'));
  //           // console.log(self.scope.txns[i].attr('balance'));
  //           // console.log();
	//
  //           // Save with the new balance
  //           self.scope.txns[i].save();
	//
  //           self.count++;
  //         } else {
  //           setTimeout(function(){
  //             // Reset the count.
  //             self.count = 0;
  //           }, 1000);
	//
  //         }
	//
  //       }
	//
  //       // Save the txns.
	// 			// for (var ii = index; ii >= 0; ii--) {
  //   //       self.scope.txns[ii].save();
	// 			// }
	// 		}
	//
	//
	// 	}
	// }
});
