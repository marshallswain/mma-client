'use strict';

import './account-balances.css!';
import appState from '../../../../../appState';

can.Component.extend({
  tag: 'account-balances',
  template: can.view('/dash/components/pages/accounts/view-account/account-balances/account-balances.stache'),
  scope: {
    appState:appState,
    select(context, el, ev){
      $('.mini-box-center.active').removeClass('active');
      el.addClass('active');
    },
    bankIcon:'<svg width="34px" height="33px" viewBox="0 0 34 33" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">'+
      '<g id="Account" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">'+
        '<g id="Short-Header" sketch:type="MSArtboardGroup" transform="translate(-362.000000, -153.000000)">'+
          '<g id="Bank-Icon" sketch:type="MSLayerGroup" transform="translate(363.000000, 155.000000)">'+
            '<path d="M31.9999982,9.16594105 L31.9999982,5.30472996 L16.0208422,0 L2.84965724e-08,5.30472996 L0,9.16594105 L3.17518197,9.16594105 L3.17518197,24.210814 L0,24.210814 L0,30 L32,30 L31.9999982,24.210814 L29.4236922,24.210814 L29.4236922,9.16594105 L31.9999982,9.16594105 Z" id="Rectangle-31" stroke="#FFFFFF" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>'+
            '<path d="M3,9 L3,24 L6,24 L6,9 L3,9 Z M9,9 L9,24 L12,24 L12,9 L9,9 Z M15,9 L15,24 L18,24 L18,9 L15,9 Z M20,9 L20,24 L23,24 L23,9 L20,9 Z M26,9 L26,24 L29,24 L29,9 L26,9 Z M32,5.25568797 L15.9742479,0 L0,5.25568797 L0,9 L32,9 L32,5.25568797 Z M-6.03961325e-14,24 L-6.03961325e-14,30 L32,30 L32,24 L-6.03961325e-14,24 Z" id="Rectangle" fill="#000000" sketch:type="MSShapeGroup"></path>'+
          '</g>'+
        '</g>'+
      '</g>'+
    '</svg>',
    pendingIcon:'<svg width="36px" height="36px" viewBox="0 0 36 36" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">'+
      '<g id="Account" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">'+
        '<g id="Short-Header" sketch:type="MSArtboardGroup" transform="translate(-507.000000, -156.000000)" stroke="#000000" fill="#000000">'+
          '<path d="M540.161504,166.30244 C541.337199,168.613549 542,171.229372 542,174 C542,183.388841 534.388841,191 525,191 C515.611159,191 508,183.388841 508,174 C508,164.611159 515.611159,157 525,157 C529.231685,157 533.102246,158.546157 536.077571,161.104359 L524.587084,175.961376 L517.739332,170.250581 L513,176.276728 L525.043016,186 L540.161504,166.30244 Z" id="pending_icon" sketch:type="MSShapeGroup"></path>'+
        '</g>'+
      '</g>'+
    '</svg>',
    reservedIcon:'<svg width="41px" height="32px" viewBox="0 0 41 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">'+
      '<g id="Account" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">'+
        '<g id="Short-Header" sketch:type="MSArtboardGroup" transform="translate(-653.000000, -154.000000)">'+
          '<g id="reserved_icon" sketch:type="MSLayerGroup" transform="translate(654.000000, 155.000000)">'+
            '<path d="M12.2109376,30 L36.0006648,30 C37.6502991,30 39,28.65 39,27 L39,3 C39,1.35 37.6502991,0 36.0006648,0 L12.2109376,0 C10.5613032,0 8.44077319,1.108 7.49898193,2.463 L0.721484104,12.21 C-0.220307156,13.564 -0.24330206,15.796 0.671495184,17.17 L7.54897085,27.503 C8.46376809,28.877 10.5613032,30 12.2109376,30 Z" id="Shape" stroke="#FFFFFF" fill="#000000" sketch:type="MSShapeGroup"></path>'+
            '<path d="M29.584625,15.0775078 C28.8720625,14.5079508 27.4518125,13.9672967 25.3279375,13.4538454 L25.3279375,10.4351935 C25.765875,10.6273127 26.220875,10.7871287 26.3776875,10.9605461 C26.5345,11.1339634 26.9789375,11.4450945 27.1739375,11.8947895 L30.5385,11.3337334 C30.2874375,10.3238324 29.7463125,9.52985299 28.9191875,8.94329433 C28.0888125,8.36353635 26.8944375,8.02945295 25.3279375,7.93679368 L25.3279375,7 L23.7971875,7 L23.7971875,7.93679368 C22.077125,8.02180218 20.783625,8.440044 19.912625,9.19406941 C19.04325,9.95319532 18.6069375,10.8942394 18.6069375,12.019752 C18.6069375,12.8443344 18.8263125,13.5405541 19.25775,14.1143614 C19.691625,14.6864686 20.2035,15.1021602 20.7958125,15.3622862 C21.3856875,15.6241124 22.3875,15.8306831 23.799625,16.1826183 L23.798,19.6373637 C23.308875,19.4205921 22.68975,19.1766177 22.476875,18.8977898 C22.2631875,18.619812 21.82525,18.1675668 21.700125,17.540204 L18,17.9864986 C18.1259375,18.6189619 18.31525,19.1545155 18.5655,19.6033603 C18.8181875,20.0471047 19.174875,20.4636464 19.64125,20.849585 C20.104375,21.2346735 20.656875,21.5407041 21.2979375,21.7574757 C21.937375,21.9767977 22.7701875,22.1221622 23.7971875,22.2063206 L23.7971875,24 L25.3279375,24 L25.3279375,22.2063206 C26.1891875,22.1748675 26.9155625,22.0737074 27.507875,21.90029 C28.102625,21.7243224 28.66325,21.4437944 29.191375,21.0612561 C29.7186875,20.6753175 30.153375,20.1933193 30.493,19.6186619 C30.832625,19.039754 31,18.4064406 31,17.7178718 C30.9991875,16.5413541 30.52875,15.8425843 29.584625,15.0775078 L29.584625,15.0775078 Z M24,13 C23.5538342,12.8778195 22.4384198,12.6973684 22.2695585,12.4633459 C22.0999225,12.2406015 22,11.8148496 22,11.5065789 C22,11.2105263 22.1363284,10.9107143 22.5995352,10.5441729 C22.8040279,10.3834586 23.5197521,10.0225564 24,10 L24,13 L24,13 Z M26.4345049,19.4619651 C26.152428,19.633122 25.4897819,19.8874802 25,20 L25.0040201,17 C25.5634837,17.2004754 26.5577879,17.5570523 26.7500826,17.7876387 C26.9423772,18.0229794 26.9999986,18.2805071 26.9999986,18.5586371 C27.0006687,18.8763867 26.7547727,19.2694136 26.4345049,19.4619651 L26.4345049,19.4619651 Z" id="Shape" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>'+
            '<path d="M9.00047048,17 C7.89767114,17 7,16.1022327 7,15.0014101 C7,13.8996475 7.89767114,13 9.00047048,13 C10.1051517,13 11,13.8996475 11,15.0014101 C11,16.1022327 10.1051517,17 9.00047048,17 Z" id="Shape" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>'+
          '</g>'+
        '</g>'+
      '</g>'+
    '</svg>',
    calendarIcon:'<svg width="38px" height="38px" viewBox="0 0 38 38" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">'+
      '<g id="Account" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">'+
        '<g id="Short-Header" sketch:type="MSArtboardGroup" transform="translate(-802.000000, -150.000000)" stroke="#FFFFFF" fill="#000000">'+
          '<path d="M807.211,151 C804.889,151 803,152.821949 803,155.061887 L803,182.938113 C803,185.178051 804.889,187 807.211,187 L834.787,187 C837.11,187 839,185.178051 839,182.938113 L839,155.061887 C839,152.820949 837.11,151 834.787,151 L807.211,151 Z M837,163 L837,182.932267 C837,184.074434 836.008,185 834.787,185 L807.211,185 C805.99,185 805,184.074434 805,182.932267 L805,163 L837,163 Z" id="calendar_icon" sketch:type="MSShapeGroup"></path>'+
        '</g>'+
      '</g>'+
    '</svg>'
  }
});
