'use strict';

can.Component.extend({
	tag: 'txn-reserved',
	template: can.view('/dash/components/pages/accounts/view-account/txn-reserved/txn-reserved.stache'),
	scope: {},
	events: {},
	helpers:{}
});
