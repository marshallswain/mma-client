'use strict';

import './view-account.css!';

import './txn-future/txn-future';
import './txn-reserved/txn-reserved';
import './txn-transactions/txn-transactions';
import './txn-bank/txn-bank';
import './account-balances/account-balances';

import appState from '../../../../appState';
import {Future} from '../../../../models';
import {Reserved} from '../../../../models';
import {Transaction} from '../../../../models';
import {BankTransaction} from '../../../../models';

var txns = new can.Map({
  future:new Future.List(),
  reserved:new Reserved.List(),
  bank:new BankTransaction.List(),
  transactions:new Transaction.List()
});

// Any time the appState.Account_ID changes, update the txns.
appState.bind('Account_ID', function(ev, accountID){
  if(accountID) {
    txns.attr('future', new Future.List());
    txns.attr('reserved', new Reserved.List());
    txns.attr('transactions', new Transaction.List({accountID:accountID, $sort:{date:-1}}));
    txns.attr('bank', new BankTransaction.List({accountID:accountID, $sort:{date:-1}}));
  }
});

can.Component.extend({
  tag: 'sc-account',
  template: can.view('/dash/components/pages/accounts/view-account/view-account.stache'),
  scope: {
    appState:appState,
    txns:txns,

    updateAccountSection(undefined, el){

      appState.removeAttr('txn_id');

      // Update the acct_section in route.
      can.route.attr('acct_section', el.attr('acct-section'));

      // Show the correct sidebar.
      can.route.attr('sidebar', 'account-' + el.attr('acct-section'));
    }
  },
  helpers:{
    isActive: function(options){
      // Get reference to the current element
      var el = $(options.nodeList[0]);
      // If the acct-section matches the route...
      if(can.route.attr('acct_section') == el.attr('acct-section')){
        el.removeClass('light');
      } else {
        el.addClass('light');
      }
    }
  }
});

