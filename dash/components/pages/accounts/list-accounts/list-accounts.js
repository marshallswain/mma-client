'use strict';

import appState from '../../../../appState';

can.Component.extend({
  tag: 'account-list',
  template: can.view('/dash/components/pages/accounts/list-accounts/list-accounts.stache'),
  scope: appState,
  events: {}
});
