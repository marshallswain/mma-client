'use strict';

import {Account} from '../../../../models';

can.Component.extend({
  tag: 'account-add',
  template: can.view('/dash/components/pages/accounts/add-account/add-account.stache'),
  scope: {
    account:new Account()
  },
  events: {

  }
});
