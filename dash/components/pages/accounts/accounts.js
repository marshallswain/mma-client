'use strict';

import './list-accounts/list-accounts';
import './add-account/add-account';
import './view-account/view-account';

can.Component.extend({
	tag: 'sc-accounts',
	template: can.view('/dash/components/pages/accounts/accounts.stache'),
	scope: {},
	events: {},
	helpers:{}
});
