'use strict';

import './quote-of-the-day/quote-of-the-day';
import './todos/todos';

can.Component.extend({
	tag: 'sc-overview',
	template: can.view('/dash/components/pages/overview/overview.stache'),
	scope: {},
	events: {},
	helpers:{}
});
