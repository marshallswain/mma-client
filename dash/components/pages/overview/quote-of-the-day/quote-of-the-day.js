'use strict';

can.Component.extend({
  tag: 'quote-of-the-day',
  template: can.view('/dash/components/pages/overview/quote-of-the-day/quote-of-the-day.stache'),
  scope: {
    name: null
  },
  events: {
    '{can.route} editvehicle': function(route, ev, newVal){
      if (newVal == 'true') {
        console.log('clicked');
      }
    }
  }
});
