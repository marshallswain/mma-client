'use strict';

can.Component.extend({
	tag: 'sc-creditscore',
	template: can.view('/dash/components/pages/overview/creditscore/creditscore.stache'),
	scope: {},
	events: {},
	helpers:{}
});
