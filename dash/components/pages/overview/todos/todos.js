'use strict';

// import appState '../../../../appState';
import {Todo} from '../../../../models';


can.Component.extend({
  tag: 'todo-list',
  template: can.view('/dash/components/pages/overview/todos/todos.stache'),
  scope: {
    todos: new Todo.List()
  },
  events: {
    'init': function(){
      var self = this;
      Todo.findAll().then(function(todos){
        self.scope.todos.attr(todos);
      });
    },
    'form submit': function(el, ev){
      // Prevent form from submitting.
      ev.preventDefault();

      var field = el.find('[name="description"]');
      var desc = field.val();

      // Create new todo.
      var todo = new Todo({description:desc});
      todo.save();

      // Reset the field.
      field.val('');
    },
    'a.delete click': function(el, ev){
      ev.preventDefault();

      var todo = el.parents('li').data('todo');
      todo.destroy();
    },
    '[name="done"] click': function(el){
      // can-value modified the todo, already, so just save.
      var todo = el.parents('li').data('todo');
      todo.save();
    },
    '{Todo} created': function(Model, ev, todo){
      this.scope.todos.push(todo);
    }
  }
});
