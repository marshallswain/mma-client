'use strict';

can.Component.extend({
	tag: 'sc-labels',
	template: can.view('/dash/components/pages/overview/labels/labels.stache'),
	scope: {},
	events: {},
	helpers:{}
});
