'use strict';

can.Component.extend({
	tag: 'sc-reports',
	template: can.view('/dash/components/pages/reports/reports.stache'),
	scope: {},
	events: {},
	helpers:{}
});
