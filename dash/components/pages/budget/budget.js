'use strict';

import './budget.css!';

can.Component.extend({
	tag: 'sc-budget',
	template: can.view('/dash/components/pages/budget/budget.stache'),
	scope: {},
	events: {},
	helpers:{}
});
