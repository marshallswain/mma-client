'use strict';

can.Component.extend({
	tag: 'sc-calendar',
	template: can.view('/dash/components/pages/calendar/calendar.stache'),
	scope: {},
	events: {},
	helpers:{}
});
