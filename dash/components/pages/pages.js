'use strict';

import appState from '../../appState';

import './overview/overview';
import './overview/todos/todos';
import './overview/creditscore/creditscore';
import './overview/labels/labels';
import './accounts/accounts';
import './calendar/calendar';
import './planning/planning';
import './budget/budget';
import './contacts/contacts';
import './reports/reports';
import './settings/settings';
import './temp/buttons';

var pages = {
  login: '<sc-login session="{session}"></sc-login>'
};


// When showPage changes, change the app's page, or show the login screen.
appState.bind('showPage', function(ev, newVal){
  if(newVal) {
    switch(newVal){
      case 'login':
        var template =  pages[newVal];
        $('#content').html(  can.stache( template )( appState ) );
        break;
      default:
        var tmplt =  pages[newVal] || '<sc-'+newVal+'></sc-'+newVal+'>';
        $('#content').html(  can.stache( tmplt )( appState ) );
    }
    // Set the ui color.
    appState.attr('color', appState.colors.attr(newVal));
  }
});
