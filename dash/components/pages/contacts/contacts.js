'use strict';

can.Component.extend({
	tag: 'sc-contacts',
	template: can.view('/dash/components/pages/contacts/contacts.stache'),
	scope: {},
	events: {},
	helpers:{}
});
