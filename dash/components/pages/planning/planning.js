'use strict';

import './planning.css!'
import './planning-progress-bar/planning-progress-bar';

can.Component.extend({
	tag: 'sc-planning',
	template: can.view('/dash/components/pages/planning/planning.stache'),
	scope: {},
	events: {},
	helpers:{}
});
