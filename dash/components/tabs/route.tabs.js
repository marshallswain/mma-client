'use strict';

can.Component.extend({
  tag: 'hello-world',
  // template: '{{#if visible}}{{message}}{{else}}Click me{{/if}}',
  scope: {
    visible: false,
    message: 'Hello There!'
  },
  events: {
    '{can.route} editvehicle': function(route, ev, newVal){
      if (newVal == 'true') {
        console.log('clicked');
      }7
    }
  }
});