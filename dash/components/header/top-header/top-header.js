'use strict';

import './top-header.css!';
import appState from '../../../../appState';

can.Component.extend({
	tag: 'top-header',
	template: can.view('/dash/components/header/top-header/top-header.stache'),
	scope: appState,
	events: {},
	helpers:{}
});
