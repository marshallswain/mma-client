'use strict';

import appState from '../../../../appState';

var bcMap = new can.Map({
	'#root':[{'name':'Overview'}],
	'overview':[{'name':'Overview'}],
	'todos':[{'name':'To-Do List'}],
	'labels':[{'name':'Labels'}],
	'creditscore':[{'name':'Credit Score'}],

	'accounts':[{'name':'Accounts'}],
	'accounts/add': [{'name':'Accounts'}, {'name':'add'}],
	'accounts/:account_id': [{'name':'Accounts'}, {'name':''}],
	'accounts/:account_id/scheduled': [{'name':'Accounts'}, {'name':''}, {'name':'Scheduled'}],
	'accounts/:account_id/scheduled/add': [{'name':'Accounts'}, {'name':''}, {'name':'Scheduled'}, {'name':'Add Transaction'}],
	'accounts/:account_id/tags': [{'name':'Accounts'}, {'name':''}, {'name':'Tags'}],
	'accounts/:account_id/tags/add': [{'name':'Accounts'}, {'name':''}, {'name':'Tags'}, {'name':'Add Transaction'}],
	'accounts/:account_id/history': [{'name':'Accounts'}, {'name':''}, {'name':'History'}],
	'accounts/:account_id/history/:txn_id': [{'name':'Accounts'}, {'name':''}, {'name':'History'}, {'name':''}],
	'accounts/:account_id/history/add': [{'name':'Accounts'}, {'name':''}, {'name':'History'}, {'name':'Add Transaction'}],
	'accounts/edit/:account_id': [{'name':'Accounts'}, {'name':'edit'}, {'name':''}],

	'calendar':[{'name':'Calendar'}],
	'budget':[{'name':'Budget'}],
	'planning':[{'name':'Planning'}],
	'contacts':[{'name':'Contacts'}],
	'reports':[{'name':'Reports & Charts'}],
	'settings':[{'name':'Settings'}],
});


can.Component.extend({
	tag: 'bread-crumbs',
	template: can.view('/dash/components/app-header/bread-crumbs/bread-crumbs.stache'),
	scope: {
		breadcrumbs:new can.List([]),
	},
	helpers:{
		bread: function(options){
			var bread = '';
			this.breadcrumbs.each(function(crumb, index){
				if (index) {
					bread += '&nbsp;:&nbsp;' + crumb.name;
				} else {
					bread += crumb.name;
				}
			});
			return bread;
		}
	},
	events: {
		/**
		 * When the route changes, use the bcMap to find the matching breadcrumbs format.
		 */
		'{can.route} route change': function(routeObj, ev, newVal){
			var route = can.route.attr('route');

			// Handle an empty route.
			if (route === '') {
				route = '#root';
			}

			// If no map exists for the current route, alert it.
			if (!bcMap[route]) {
				window.alert('A bread-crumb is needed for ' + route);
			}
			// var data = bcMap.attr(route);
			// context.crumbs.replace(data);
		},

		'{context} account': function(routeObj, ev, newVal){
			var route = can.route.attr('route');

			bcMap['accounts/:account_id'][1].attr('name', context.account.attr('name'));
			bcMap['accounts/:account_id/history'][1].attr('name', context.account.attr('name'));
			bcMap['accounts/:account_id/history/:txn_id'][1].attr('name', context.account.attr('name'));
			bcMap['accounts/:account_id/history/add'][1].attr('name', context.account.attr('name'));
			bcMap['accounts/:account_id/tags'][1].attr('name', context.account.attr('name'));
			bcMap['accounts/:account_id/tags/add'][1].attr('name', context.account.attr('name'));
			bcMap['accounts/:account_id/scheduled'][1].attr('name', context.account.attr('name'));
			bcMap['accounts/:account_id/scheduled/add'][1].attr('name', context.account.attr('name'));

			// This one is different.
			bcMap['accounts/edit/:account_id'][2].attr('name', context.account.attr('name'));

			// Pull the data again.
			var data = bcMap.attr(route);

			// Update the breadcrumbs.
			context.crumbs.replace(data);
		},

		'{context} txn': function(routeObj, ev, newVal){
			var route = can.route.attr('route');

			bcMap['accounts/:account_id/history/:txn_id'][3].attr('name', context.txn.attr('description'));

			// Pull the data again.
			var data = bcMap.attr(route);

			// Update the breadcrumbs.
			context.crumbs.replace(data);
		}
	}
});
