'use strict';

import appState from '../../../../appState';

can.Component.extend({
  tag: 'bc-reports',
  template: can.view('/dash/components/header/bread-crumbs/bc-reports/bc-reports.stache'),
  scope: appState,
  helpers:{},
  events: {}
});
