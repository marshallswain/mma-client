'use strict';

import appState from '../../../../appState';

import {planningSections} from '../../../../dictionaries/planningSections';

can.Component.extend({
  tag: 'bc-planning',
  template: can.view('/dash/components/header/bread-crumbs/bc-planning/bc-planning.stache'),
  scope: appState,
  helpers:{
    sectionName:function(section){
      return planningSections[section()];
    }
  }
});
