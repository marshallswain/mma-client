'use strict';

import appState from '../../../../appState';

can.Component.extend({
  tag: 'bc-overview',
  template: can.view('/dash/components/header/bread-crumbs/bc-overview/bc-overview.stache'),
  scope: appState,
  helpers:{},
  events: {}
});
