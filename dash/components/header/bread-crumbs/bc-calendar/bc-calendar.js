'use strict';

import appState from '../../../../appState';

can.Component.extend({
  tag: 'bc-calendar',
  template: can.view('/dash/components/header/bread-crumbs/bc-calendar/bc-calendar.stache'),
  scope: appState,
  helpers:{},
  events: {}
});
