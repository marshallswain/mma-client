'use strict';

import appState from '../../../../appState';

can.Component.extend({
  tag: 'bc-settings',
  template: can.view('/dash/components/header/bread-crumbs/bc-settings/bc-settings.stache'),
  scope: appState,
  helpers:{},
  events: {}
});
