'use strict';

import appState from '../../../../appState';

can.Component.extend({
  tag: 'bc-account',
  template: can.view('/dash/components/header/bread-crumbs/bc-account/bc-account.stache'),
  scope: appState,
  helpers:{},
  events: {}
});
