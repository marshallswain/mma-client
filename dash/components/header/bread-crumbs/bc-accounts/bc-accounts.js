'use strict';

import appState from '../../../../appState';

can.Component.extend({
  tag: 'bc-accounts',
  template: can.view('/dash/components/header/bread-crumbs/bc-accounts/bc-accounts.stache'),
  scope: appState,
  helpers:{},
  events: {}
});
