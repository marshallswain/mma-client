'use strict';

import appState from '../../../../appState';

can.Component.extend({
  tag: 'bc-contacts',
  template: can.view('/dash/components/header/bread-crumbs/bc-contacts/bc-contacts.stache'),
  scope: appState,
  helpers:{},
  events: {}
});
