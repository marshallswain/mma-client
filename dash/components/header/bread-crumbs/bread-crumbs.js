'use strict';

import appState from '../../../../appState';

import './bc-overview/bc-overview';
import './bc-account/bc-account';
import './bc-accounts/bc-accounts';
import './bc-calendar/bc-calendar';
import './bc-budget/bc-budget';
import './bc-planning/bc-planning';
import './bc-contacts/bc-contacts';
import './bc-reports/bc-reports';
import './bc-settings/bc-settings';


can.Component.extend({
	tag: 'bread-crumbs',
	template: can.view('/dash/components/header/bread-crumbs/bread-crumbs.stache'),
	scope: appState,
	helpers:{},
	events: {}
});

// Put the breadcrumbs in place when the page changes.
appState.bind('page', function(ev, newVal, oldVal){
	if(newVal && newVal != oldVal) {
		var tmplt =  '<bc-'+newVal+'></bc-'+newVal+'>';
		$('#pagecrumbs').html(  can.stache( tmplt )( appState ) );
	}
});
