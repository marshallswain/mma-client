'use strict';

import appState from '../../../../appState';

can.Component.extend({
  tag: 'bc-budget',
  template: can.view('/dash/components/header/bread-crumbs/bc-budget/bc-budget.stache'),
  scope: appState,
  helpers:{},
  events: {}
});
