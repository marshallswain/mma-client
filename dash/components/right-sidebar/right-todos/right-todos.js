'use strict';

can.Component.extend({
  tag: 'right-todos',
  template: can.view('/dash/components/right-sidebar/right-todos/right-todos.stache'),
  scope: {},
  events: {
    'init': function(el){}
  }
});