'use strict';

import '../../../appState';
import '../../../models';
// var Account = models.Account;

can.Component.extend({
  tag: 'right-account-add',
  template: can.view('/dash/components/right-sidebar/right-account-add/right-account-add.stache'),
  scope: {
    name: null
  },
  events: {
    'submit': function(el, ev){
      ev.preventDefault();

      // Get the form data.
      var data = el.formParams();

      // Create a new account.
      var account = new Account(data);
      account.save();

      can.route.attr({'view':'accounts', 'sidebar':'accounts'});
    },
    '.cancel click':function(el, ev){
      can.route.attr({'view':'accounts', 'sidebar':'accounts'});
    }
  }
});