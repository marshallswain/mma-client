'use strict';

import './right-sidebar.css!';
import appState from '../../appState';

/* * * Sidebar Components * * */
import './right-overview/right-overview';
import './right-todos/right-todos';
import './right-labels/right-labels';
import './right-creditscore/right-creditscore';
import './right-accounts/right-accounts';
import './right-account/right-account';
import './right-account-future/right-account-future';
import './right-account-history/right-account-history';
import './right-account-reserved/right-account-reserved';
import './right-account-add/right-account-add';
import './right-account-edit/right-account-edit';
import './right-txn/right-txn';
import './right-txn-history-add/right-txn-history-add';
import './right-calendar/right-calendar';
import './right-budget/right-budget';
import './right-planning/right-planning';
import './right-contacts/right-contacts';
import './right-reports/right-reports';
import './right-settings/right-settings';

import './sidebar-selector/sidebar-selector';

can.Component.extend({
  tag: 'right-sidebar',
  template: can.view('/dash/components/right-sidebar/right-sidebar.stache'),
  scope: {
    name: null
  }
});

// When the sidebar changes, show it.
appState.bind('sidebar', function(ev, sidebar, oldVal){
  if(sidebar && sidebar != oldVal) {
    // Show it in #right
    var tmplt =  '<right-'+sidebar+'></right-'+sidebar+'>';
    $('#right').html(  can.stache( tmplt )( appState ) );
  }
});

// When the txn_id changes, show the txn sidebar.
appState.bind('txn_id', function(ev, txn, oldVal){
  if(txn && txn != oldVal) {
    appState.attr('sidebar', 'txn');
  }
});

// When the page changes, show the page's DefaultSidebar from prefs.
appState.bind('page', function(ev, newVal, oldVal){
  if(newVal && newVal != oldVal) {
    // Retrieve the default right sidebar and show it in #right
    var sidebar = appState.attr('prefs.DefaultSidebars.' + newVal);//
    var tmplt =  '<right-'+sidebar+'></right-'+sidebar+'>';
    $('#right').html(  can.stache( tmplt )( appState ) );
  }
});
