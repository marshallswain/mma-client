'use strict';

import appState from '../../../appState';

can.Component.extend({
  tag: 'right-txn',
  template: can.view('/dash/components/right-sidebar/right-txn/right-txn.stache'),
  scope: {
    appState: appState,

    // Example right-click
    sayHi(context, el, ev){
      alert('hi!');
      ev.preventDefault();
    }
  },
  events: {
    'close':function(){
      can.route.attr({'sidebar':'account-history'});
      can.route.removeAttr('txn_id');
    },
    'submit': function(el, ev){
      ev.preventDefault();

      // Get the data.
      var data = el.formParams();
      delete data.reconciled;
      delete data.balance;
      appState.txn.attr(data);
      appState.txn.save();

      this.close();
    },
    '.cancel click':function(el, ev){
      this.close();
    },
    '{appState.txn} reconciled': function(txn, ev, newVal){
      txn.save();
    },
    '.delete click':function(el, ev){
      appState.txn.destroy();
      this.close();
    }
  }
});