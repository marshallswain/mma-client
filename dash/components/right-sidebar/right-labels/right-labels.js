'use strict';

can.Component.extend({
  tag: 'right-labels',
  template: can.view('/dash/components/right-sidebar/right-labels/right-labels.stache'),
  scope: {},
  events: {
    'init': function(el){}
  }
});