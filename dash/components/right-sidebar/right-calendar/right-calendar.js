'use strict';

can.Component.extend({
  tag: 'right-calendar',
  template: can.view('/dash/components/right-sidebar/right-calendar/right-calendar.stache'),
  scope: {
    name: null
  },
  events: {
    '{can.route} editvehicle': function(route, ev, newVal){
      if (newVal == 'true') {
        console.log('clicked');
      }
    }
  }
});