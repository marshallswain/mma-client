'use strict';

can.Component.extend({
  tag: 'right-reports',
  template: can.view('/dash/components/right-sidebar/right-reports/right-reports.stache'),
  scope: {},
  events: {
    'init': function(el){}
  }
});