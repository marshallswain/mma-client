'use strict';

can.Component.extend({
  tag: 'right-settings',
  template: can.view('/dash/components/right-sidebar/right-settings/right-settings.stache'),
  scope: {},
  events: {
    'init': function(el){}
  }
});