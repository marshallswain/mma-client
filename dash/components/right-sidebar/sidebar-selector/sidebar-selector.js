'use strict';

import './sidebar-selector.css!';

can.Component.extend({
  tag: 'right-selector',
  template: can.view('/dash/components/right-sidebar/sidebar-selector/sidebar-selector.stache'),
  scope: {
    toggle(scope, nodeList){
      var el = $(nodeList[0]);
      el.parent().toggleClass('open');
      var ul = el.siblings('ul');
      ul.slideToggle(200);
    }
  },
  events: {
    'button click': function(el){
      el.toggleClass('active');
      el.next().toggle();
      if (el.next().css('display') == 'none') {
        el.blur();
      }
    }
    // 'button blur': function(el, ev){
    //   el.next().hide();
    // }
  }
});
