'use strict';

can.Component.extend({
  tag: 'right-account-history',
  template: can.view('/dash/components/right-sidebar/right-account-history/right-account-history.stache'),
  scope: {
    name: null
  },
  events: {
    '{can.route} editvehicle': function(route, ev, newVal){
      if (newVal == 'true') {
        console.log('clicked');
      }
    }
  }
});
