'use strict';

can.Component.extend({
  tag: 'right-contacts',
  template: can.view('/dash/components/right-sidebar/right-contacts/right-contacts.stache'),
  scope: {},
  events: {
    'init': function(el){}
  }
});