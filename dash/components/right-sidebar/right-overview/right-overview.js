'use strict';

can.Component.extend({
  tag: 'right-overview',
  template: can.view('/dash/components/right-sidebar/right-overview/right-overview.stache'),
  scope: {
    name: null
  },
  events: {
    '{can.route} editvehicle': function(route, ev, newVal){
      if (newVal == 'true') {
        console.log('clicked');
      }
    }
  }
});