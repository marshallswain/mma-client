'use strict';

import Transaction from '../../../models';
import appState from '../../../appState';

can.Component.extend({
  tag: 'right-txn-history-add',
  template: can.view('/dash/components/right-sidebar/right-txn-history-add/right-txn-history-add.stache'),
  scope: appState,
  events: {
    'submit': function(el, ev){
      ev.preventDefault();

      var data = el.formParams();

      var txn = new Transaction(data);
      txn.save();

      // can.route.removeAttr('account_id');

      // Call close on the nearest .right-sidebar.
      can.route.attr({'sidebar':'account'});
    },
    '.cancel click':function(el, ev){
      // can.route.removeAttr('account_id');

      can.route.attr({'sidebar':'account'});
    }
  }
});

