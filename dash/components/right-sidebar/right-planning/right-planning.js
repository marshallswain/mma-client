'use strict';

can.Component.extend({
  tag: 'right-planning',
  template: can.view('/dash/components/right-sidebar/right-planning/right-planning.stache'),
  scope: {},
  events: {
    'init': function(el){}
  }
});