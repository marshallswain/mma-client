'use strict';

can.Component.extend({
  tag: 'right-creditscore',
  template: can.view('/dash/components/right-sidebar/right-creditscore/right-creditscore.stache'),
  scope: {},
  events: {
    'init': function(el){}
  }
});