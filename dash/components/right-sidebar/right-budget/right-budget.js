'use strict';

can.Component.extend({
  tag: 'right-budget',
  template: can.view('/dash/components/right-sidebar/right-budget/right-budget.stache'),
  scope: {},
  events: {
    'init': function(el){}
  }
});