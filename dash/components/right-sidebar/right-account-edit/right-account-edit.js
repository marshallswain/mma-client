'use strict';

import appState from '../../../appState';

can.Component.extend({
  tag: 'right-account-edit',
  template: can.view('/dash/components/right-sidebar/right-account-edit/right-account-edit.stache'),
  scope: appState,
  events: {
    'submit': function(el, ev){
      ev.preventDefault();

      var data = el.formParams();
      this.scope.context.account.attr(data);

      console.log(this.scope.context.account.attr());
      this.scope.context.account.save();

      can.route.removeAttr('account_id');

      // Call close on the nearest .right-sidebar.
      can.route.attr({'view':'accounts', 'sidebar':'accounts'});
    },
    '.cancel click':function(el, ev){
      // can.route.removeAttr('account_id');

      can.route.attr({'view':'accounts', 'sidebar':'accounts'});
    }
  }
});

