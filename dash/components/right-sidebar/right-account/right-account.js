'use strict';

can.Component.extend({
  tag: 'right-account',
  template: can.view('/dash/components/right-sidebar/right-account/right-account.stache'),
  scope: {
    name: null
  },
  events: {
    '{can.route} editvehicle': function(route, ev, newVal){
      if (newVal == 'true') {
        console.log('clicked');
      }
    }
  }
});
