'use strict';

can.Component.extend({
  tag: 'right-accounts',
  template: can.view('/dash/components/right-sidebar/right-accounts/right-accounts.stache'),
  scope: {
    name: null
  },
  events: {
    '{can.route} editvehicle': function(route, ev, newVal){
      if (newVal == 'true') {
        console.log('clicked');
      }
    }
  }
});