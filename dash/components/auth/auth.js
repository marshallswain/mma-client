import './login/login';
import './signup/signup';
import './verify/verify';
import './passwordchangesuccess/passwordchangesuccess';
import './passwordchange/passwordchange';
import './passwordemail/passwordemail';
import './my-account/my-account';
