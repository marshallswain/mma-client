'use strict';

import '../change-password/change-password';

can.Component.extend({
	tag: 'sc-my-account',
	template: can.view('/dash/components/auth/my-account/my-account.stache'),
	scope: {}
});