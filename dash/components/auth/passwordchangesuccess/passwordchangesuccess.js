'use strict';

can.Component.extend({
	tag: 'sc-passwordchangesuccess',
	template: can.view('/dash/components/auth/passwordchangesuccess/passwordchangesuccess.stache'),
});
