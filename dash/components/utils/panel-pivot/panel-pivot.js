'use strict';

import './panel-pivot.css!';

can.Component.extend({
  tag: 'panel-pivot',
  scope: {
    pivot:function(undefined, el, ev){
      el.toggleClass('open');
      el.parents('.panel').find('.panel-body').slideToggle(200);
      ev.stopPropagation();
    }
  },
  helpers:{
    // Open the panel corresponding to the route's 'section'.
    pivotState:function(options){
      var el = $(options.nodeList[0]);
      var panelBody = el.find('.panel-body');
      var pivotHandle = el.find('.pivot-handle');
      if (can.route.attr('section') == this.attr('section') ){
        pivotHandle.addClass('open');
        panelBody.slideDown(200);
      } else {
        pivotHandle.removeClass('open');
        panelBody.slideUp(200);
      }
    }
  }
});
