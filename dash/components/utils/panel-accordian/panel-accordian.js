'use strict';

import './panel-accordian.css!';

can.Component.extend({
  tag: 'panel-accordian',
  scope: {},
  helpers:{
    // Open the panel corresponding to the route's 'section'.
    pivotState:function(options){
      var el = $(options.nodeList[0]);
      var panelHeading = el.find('.panel-heading');
      var panelBody = el.find('.panel-body');
      if (can.route.attr('section') == this.attr('section') ){
        panelHeading.addClass('open');
        panelBody.slideDown(200);
      } else {
        panelHeading.removeClass('open');
        panelBody.slideUp(200);
      }
    }
  }
});
