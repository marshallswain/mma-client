'use strict';

window.mobile = function(){
	$('#left').css({'width':'auto', 'right':'100%'});
	$('#content').css({'width':'auto', 'left':'0', 'right':'0', 'padding-right':'0'});
	$('#right').css({'width':'auto', 'left':'100%'});
};
window.desktop = function(){
	$('#left').removeAttr('style');
	$('#content').removeAttr('style');
	$('#right').removeAttr('style');
};
window.one = function(){
	$('#left').css({'left':0, 'right':0, 'width':'auto'});
	$('#content').css({'left':'100%', 'right':'-100%', 'width':'auto'});
	$('#right').css({'left':'200%', 'right':'-200%', 'width':'auto'});
};
window.two = function(){
	$('#left').css({'left':'-100%', 'right':'100%', 'width':'auto'});
	$('#content').css({'left':'0', 'right':'0', 'width':'auto'});
	$('#right').css({'left':'100%', 'right':'-100%', 'width':'auto'});
};
window.three = function(){
	$('#left').css({'left':'-200%', 'right':'100%', 'width':'auto'});
	$('#content').css({'left':'-100%', 'right':'200%', 'width':'auto'});
	$('#right').css({'left':'0', 'right':'0', 'width':'auto'});
};