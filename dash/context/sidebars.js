'use strict';




// An array to hold the history of the sidebar.
var history = new can.List([]);

// When the route sidebar changes, push it to the history.
can.route.bind('sidebar', function(ev, newVal, oldVal) {

  // Get the corresponding tag name from the defaults
  var sidebar = defaultsMap[newVal];

  // Push it to the history.
  history.push(sidebar);
});

exports.history = history;

window.hist = history;
