/* * * Auth * * */
can.route('passwordemail/:email',{page: 'passwordemail'});
can.route('passwordchange/:secret',{page: 'passwordchange'});
can.route('verify/:secret',{page: 'verify'});
// can.route(':page',{page: 'overview'});


/* * * Overview * * */
can.route('', {'page':'overview'});
can.route('overview', {'page':'overview'});
can.route('todos', {'page':'todos','sidebar':'todos'});
can.route('labels', {'page':'labels','sidebar':'labels'});
can.route('creditscore', {'page':'creditscore','sidebar':'creditscore'});


/* * * Accounts * * */
can.route('accounts', {'page':'accounts', 'sidebar':'accounts'});
can.route('accounts/add', {'page':'accounts', 'sidebar':'account-add'});
can.route('accounts/:account_id', {'page':'account', 'sidebar':'account'});
can.route('accounts/edit/:account_id', {'page':'accounts', 'sidebar':'account-edit'});

can.route('accounts/:account_id/future', {'page':'account','sidebar':'account-future', 'acct_section':'future'});
can.route('accounts/:account_id/future/add', {'page':'account', 'acct_section':'future'});
can.route('accounts/:account_id/future/:scheduled_txn_id', {'page':'account', 'acct_section':'future'});

can.route('accounts/:account_id/reserved', {'page':'account', 'sidebar':'account-reserved', 'acct_section':'reserved'});
can.route('accounts/:account_id/reserved/add', {'page':'account', 'acct_section':'reserved'});
can.route('accounts/:account_id/reserved/:txn_id', {'page':'account', 'acct_section':'reserved'});

can.route('accounts/:account_id/history', {'page':'account','sidebar':'account-history', 'acct_section':'history'});
can.route('accounts/:account_id/history/add', {'page':'account', 'acct_section':'history'});
can.route('accounts/:account_id/history/:txn_id', {'page':'account', 'sidebar':'txn', 'acct_section':'history'});


/* * * Calendar * * */
can.route('calendar', {'page':'calendar', 'sidebar':'calendar'});


/* * * Budget * * */
can.route('budget', {'page':'budget', 'sidebar':'budget'});


/* * * Planning * * */
can.route('planning', {'page':'planning', 'sidebar':'planning'});
can.route('planning/:section', {'page':'planning', 'sidebar':'planning'});


/* * * Contacts * * */
can.route('contacts', {'page':'contacts', 'sidebar':'contacts'});


/* * * Reports * * */
can.route('reports', {'page':'reports', 'sidebar':'reports'});


/* * * Settings * * */
can.route('settings', {'page':'settings', 'sidebar':'settings'});
can.route('settings/:group', {'page':'settings', 'sidebar':'settings'});

can.route('buttons', {'page':'buttons', 'sidebar':'settings'});
